# 乐淘后端

这是乐淘的后端。使用*[Spring框架](http://spring.io)*。

## 核心组件

离了他们就很难说自己是在用Spring框架。

- **Spring Core** —— 专门负责那些*很重要*但你又*看不见*的东西。
- **Spring MVC** —— Spring自己的前端框架。***Thymeleaf***模板引擎依赖它。
- **Spring Boot MVC** —— 帮助我们快速使用Spring开发的组件。

## 已集成的组件

- **Web** —— Tomcat
- **JPA** —— Hibernate的CRUDRepository
- **MySQL** Dialect —— 搭配CRUDRepository食用
- **Rest Repositories** —— *解析json*并*序列化对象*
- **Thymeleaf** —— Spring官方推荐使用的模板引擎
- **Spring Security Core** —— 包含了常用的加密库、加解密方法和认证方法
- **Javax.mail** —— SMTP邮件发送模块

## 配置文件

1. ***pom.xml*** : 使用maven管理的依赖列表
2. ***letao.iml*** : idea生成的配置文件，不用管它
3. */src/main/resources*/***application.yml*** : Spring Boot的配置文件，包含了数据库连接信息、Tomcat端口等。