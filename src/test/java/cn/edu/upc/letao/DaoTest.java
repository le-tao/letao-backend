package cn.edu.upc.letao;

import cn.edu.upc.letao.dao.CommonMessageDao;
import cn.edu.upc.letao.service.MessageService;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by 风飞扬 on 2017/4/26.
 * 测试类
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DaoTest {


    @Autowired
    private CommonMessageDao messageDao;
    @Autowired
    private MessageService messageService;

/*
    @Test
    public void test1(){
        //测试数据 消息id
        int mid = 1;
        //调用dao中的查询指定消息id的消息
        CommonMessage msg = messageDao.findByMid(mid);
        String content = msg.getText();
        //测试结果是否正确
        Assert.assertEquals("这是乱写的", content);
    }
    @Test
    public void test2(){
        //测试数据 池子id
        int poolid = 1;
        //调用service中的查询一条消息的方法
        CommonMessageCustom msgCustom = messageService.queryOneMessage(poolid);
        int testPoolid = msgCustom.getPoolid();
        //测试结果是否正确
        Assert.assertEquals(poolid, testPoolid);
    }*/

}
