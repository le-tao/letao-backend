<<<<<<< HEAD
package cn.edu.upc.letao.service;

import cn.edu.upc.letao.dao.CommonMemberDao;
import cn.edu.upc.letao.entity.CommonMember;
import cn.edu.upc.letao.entity.CommonUsergroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by luxin on 2017/4/13.
 */
@Service
@Transactional
public class MemberService {
    @Autowired
    private CommonMemberDao commonMemberDao;
    @Autowired
    private HttpSession httpSession;
    @Autowired
    private GroupService groupService;

    public int getAdminId(int uid) {
        CommonMember commonMember = null;
        commonMember = commonMemberDao.findByUid(uid);
        int adminid = commonMember.getAdminid();
        return adminid;
    }

    //name代表 UID、邮箱或 用户名
    public List<CommonMember> getAllMembers(int page, String name){
        System.out.println(page);
        page--;
        page = page * 20;
        if(name != null){
            return commonMemberDao.findAndFilter(name,page);
        }else{
            return commonMemberDao.findPageable(page);
        }
    }

    //会员数量
    public Long getNum(){
        return commonMemberDao.countAll();
    }

    //根据金币 获取top5的用户
    public List<CommonMember> getTop5Memeber(){
        return commonMemberDao.getTop5Memebers();
    }

    public boolean addMember(CommonMember commonMember){
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        commonMember.setPassword(passwordEncoder.encode(commonMember.getPassword()));
        this.saveUserAndGroupid(commonMember);
        return true;
    }

    public boolean modifyMember(CommonMember commonMember){
        CommonMember theOne = commonMemberDao.findOneByUid(commonMember.getUid());
        if(theOne == null){
            return false;
        }else{
            //新对象的密码不为空
            if(commonMember.getPassword() != null){
                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                commonMember.setPassword(passwordEncoder.encode(commonMember.getPassword()));
            }else {
                //新对象的密码为空
                commonMember.setPassword(theOne.getPassword());
            }
            this.saveUserAndGroupid(commonMember);
            return true;
        }
    }

    public boolean delete(int id){
        commonMemberDao.delete(id);
        return true;
    }

    /*
    获得未审核的用户
     */
    public List<CommonMember> getUncheck(){
        return commonMemberDao.findUncheck();
    }

    public Long getUncheckNum(){
        return commonMemberDao.countUncheck();
    }

    /*
    审核用户,0通过,1删除,3不通过
     */
    public boolean check(int id,int status){
        CommonMember theOne = commonMemberDao.findOne(id);
        if(theOne == null){
            return false;
        }else{
            theOne.setStatus(status);
            commonMemberDao.save(theOne);
            return true;
        }
    }

    /*
    所有管理员
     */
    public List<CommonMember> getAdmin(){
        return commonMemberDao.findAllAdmin();
    }

    /*
    撤销管理员
     */
    public boolean removeAdmin(int id){
        CommonMember theOne = commonMemberDao.findOne(id);
        if(theOne == null || theOne.getAdminid() == 0){
            return false;
        }else{
            theOne.setAdminid(0);
            commonMemberDao.save(theOne);
            return true;
        }
    }

    /*
    修改管理员
     */
    public boolean editAdmin(CommonMember cm){
        CommonMember theOne = commonMemberDao.findOne(cm.getUid());
        if(theOne == null || theOne.getAdminid() == 0){
            return false;
        }else{
            if(cm.getPassword()!=null){
                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                cm.setPassword(passwordEncoder.encode(cm.getPassword()));
            }
            this.saveUserAndGroupid(cm);
            return true;
        }
    }

    /*
    添加管理员
     */
    public boolean addAdmin(CommonMember cm) {
        if (cm.getUid() != 0 || cm.getAdminid() != 1) {
            return false;
        } else {
            this.saveUserAndGroupid(cm);
            return true;
        }
    }

    /**
     * 保存用户的时候  根据积分自动判断该用户属于哪个群组并保存
     * @param member
     * @return
     */
    public boolean saveUserAndGroupid(CommonMember member){
        if(member == null){
            return false;
        }
        CommonUsergroup group = groupService.getProperGroup(member.getExp());
        member.setGroupid(group.getGroupid());
        commonMemberDao.save(member);
        return true;
    }

}
=======
package cn.edu.upc.letao.service;

import cn.edu.upc.letao.dao.CommonMemberDao;
import cn.edu.upc.letao.entity.CommonMember;
import cn.edu.upc.letao.entity.CommonUsergroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by luxin on 2017/4/13.
 */
@Service
@Transactional
public class MemberService {
    @Autowired
    private CommonMemberDao commonMemberDao;
    @Autowired
    private HttpSession httpSession;
    @Autowired
    private GroupService groupService;

    public int getAdminId(int uid) {
        CommonMember commonMember = null;
        commonMember = commonMemberDao.findByUid(uid);
        int adminid = commonMember.getAdminid();
        return adminid;
    }

    //name代表 UID、邮箱或 用户名
    public List<CommonMember> getAllMembers(int page, String name){
        System.out.println(page);
        page--;
        page = page * 20;
        if(name != null){
            return commonMemberDao.findAndFilter(name,page);
        }else{
            return commonMemberDao.findPageable(page);
        }
    }

    //会员数量
    public Long getNum(){
        return commonMemberDao.countAll();
    }

    //根据金币 获取top5的用户
    public List<CommonMember> getTop5Memeber(){
        return commonMemberDao.getTop5Memebers();
    }

    public boolean addMember(CommonMember commonMember){
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        commonMember.setPassword(passwordEncoder.encode(commonMember.getPassword()));
        this.saveUserAndGroupid(commonMember);
        return true;
    }

    public boolean modifyMember(CommonMember commonMember){
        CommonMember theOne = commonMemberDao.findOneByUid(commonMember.getUid());
        if(theOne == null){
            return false;
        }else{
            //新对象的密码不为空
            if(commonMember.getPassword() != null){
                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                commonMember.setPassword(passwordEncoder.encode(commonMember.getPassword()));
            }else {
                //新对象的密码为空
                commonMember.setPassword(theOne.getPassword());
            }
            this.saveUserAndGroupid(commonMember);
            return true;
        }
    }

    public boolean delete(int id){
        commonMemberDao.delete(id);
        return true;
    }

    /*
    获得未审核的用户
     */
    public List<CommonMember> getUncheck(){
        return commonMemberDao.findUncheck();
    }

    public Long getUncheckNum(){
        return commonMemberDao.countUncheck();
    }

    /*
    审核用户,0通过,1删除,3不通过
     */
    public boolean check(int id,int status){
        CommonMember theOne = commonMemberDao.findOne(id);
        if(theOne == null){
            return false;
        }else{
            theOne.setStatus(status);
            commonMemberDao.save(theOne);
            return true;
        }
    }

    /*
    所有管理员
     */
    public List<CommonMember> getAdmin(){
        return commonMemberDao.findAllAdmin();
    }

    /*
    撤销管理员
     */
    public boolean removeAdmin(int id){
        CommonMember theOne = commonMemberDao.findOne(id);
        if(theOne == null || theOne.getAdminid() == 0){
            return false;
        }else{
            theOne.setAdminid(0);
            commonMemberDao.save(theOne);
            return true;
        }
    }

    /*
    修改管理员
     */
    public boolean editAdmin(CommonMember cm){
        CommonMember theOne = commonMemberDao.findOne(cm.getUid());
        if(theOne == null || theOne.getAdminid() == 0){
            return false;
        }else{
            if(cm.getPassword()!=null){
                BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
                cm.setPassword(passwordEncoder.encode(cm.getPassword()));
            }
            this.saveUserAndGroupid(cm);
            return true;
        }
    }

    /*
    添加管理员
     */
    public boolean addAdmin(CommonMember cm) {
        if (cm.getUid() != 0 || cm.getAdminid() != 1) {
            return false;
        } else {
            this.saveUserAndGroupid(cm);
            return true;
        }
    }

    /**
     * 保存用户的时候  根据积分自动判断该用户属于哪个群组并保存
     * @param member
     * @return
     */
    public boolean saveUserAndGroupid(CommonMember member){
        if(member == null){
            return false;
        }
        CommonUsergroup group = groupService.getProperGroup(member.getExp());
        member.setGroupid(group.getGroupid());
        commonMemberDao.save(member);
        return true;
    }

}
>>>>>>> 656d1e0fc23ec798a8dba36f017a70cd2371a127
