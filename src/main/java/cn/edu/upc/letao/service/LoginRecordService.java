package cn.edu.upc.letao.service;

import cn.edu.upc.letao.dao.LoginRecordDao;
import cn.edu.upc.letao.entity.CommonLoginRecord;
import cn.edu.upc.letao.util.MyTimeTransfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by 风飞扬 on 2017/6/8.
 */
@Service
@Transactional
public class LoginRecordService {

    @Autowired
    private LoginRecordDao loginRecordDao;

    //查找某用户 今天是否已经登录过了
    public boolean isTodayLogin(Integer uid){
        if(uid == null){
            return false;
        }
        int todaytime = MyTimeTransfer.getTimesmorning();
        List<CommonLoginRecord> list = loginRecordDao.getLoginRecordsByUidAndTime(uid, todaytime);
        if(list != null){
            return true;
        }else {
            return false;
        }
    }

    //增加一条登录记录
    public void addLoginRecord(Integer uid, String token){
        if(uid!=null && token!=null){
            CommonLoginRecord clr = new CommonLoginRecord();
            clr.setUid(uid);
            clr.setTime(MyTimeTransfer.getCurrentTimeStamp());
            clr.setToken(token);

            loginRecordDao.save(clr);
        }
    }
}
