package cn.edu.upc.letao.service;

import cn.edu.upc.letao.dao.CommonMemberDao;
import cn.edu.upc.letao.entity.CommonMember;
import cn.edu.upc.letao.model.SessionUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by youweirong on 2017/4/11.
 */
@Service
public class UserinfoService {
	@Autowired
	private CommonMemberDao commonMemberDao;
	@Autowired
	private HttpSession httpSession;

	public CommonMember getCurrentUser() {
		CommonMember commonMember = null;
		if(httpSession.getAttribute("visit_user_id")!=null) {//根据当前登录的用户的id查询个人信息
			commonMember = commonMemberDao.findOneByUid((int)httpSession.getAttribute("visit_user_id"));
		}
		return commonMember;
	}

	public CommonMember editCurrentUser() {
		CommonMember commonMember = null;
		if(httpSession.getAttribute("visit_user_id")!=null) {//根据当前登录的用户的修改个人信息
			commonMember = commonMemberDao.findOneByUid((int)httpSession.getAttribute("visit_user_id"));
		}
		return commonMember;
	}

	public CommonMember editCurrentPassword() {
		CommonMember commonMember = null;
		if(httpSession.getAttribute("visit_user_id")!=null) {//根据当前登录的用户的修改密码
			commonMember = commonMemberDao.findOneByUid((int)httpSession.getAttribute("visit_user_id"));
		}
		return commonMember;
	}

	public CommonMember editCurrentFilepath(){
		CommonMember commonMember = null;
		if(httpSession.getAttribute("visit_user_id")!=null){//
			commonMember = commonMemberDao.findOneByUid((int)httpSession.getAttribute("visit_user_id"));
		}
		return commonMember;
	}
}
