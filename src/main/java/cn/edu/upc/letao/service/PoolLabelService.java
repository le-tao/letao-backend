package cn.edu.upc.letao.service;

import cn.edu.upc.letao.dao.PoolLabelDao;
import cn.edu.upc.letao.entity.PoolLabel;
<<<<<<< HEAD
=======
import cn.edu.upc.letao.redisRepository.LabelRepository;
>>>>>>> 656d1e0fc23ec798a8dba36f017a70cd2371a127
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by 风飞扬 on 2017/6/2.
 */
@Service
@Transactional
public class PoolLabelService {
    @Autowired
    private PoolLabelDao poolLabelDao;

<<<<<<< HEAD
    //添加标签or修改标签
    public void add(PoolLabel pl){
        if(pl != null){
            poolLabelDao.save(pl);
=======
    @Autowired
    private LabelRepository labelRepository;

    //添加标签or修改标签
    public void add(PoolLabel pl){
        if(pl != null){
            PoolLabel temp = poolLabelDao.save(pl);//存到数据库
            labelRepository.saveLabel(temp);//存到缓存中去
>>>>>>> 656d1e0fc23ec798a8dba36f017a70cd2371a127
        }
    }

    //根据标签id查询标签
    public PoolLabel getById(Integer id){
<<<<<<< HEAD
        if(id==null){
            return null;
        }
        return poolLabelDao.findOne(id);
    }
    //查询所有的标签
    public Object findAll(){
        return poolLabelDao.findAll();
    }
    //删除池子标签
    public boolean delete(Integer id){
=======
        return getOne(id);
    }
    //查询所有的标签
    public Object findAll(){
        Object obj = labelRepository.findAllLabels();//从redis缓存中查询
        if(obj!=null){//在redis缓存中查到
            return obj;
        }else{//如果没有查到则去数据库中查询
            Iterable<PoolLabel> all = poolLabelDao.findAll();
            if (all != null) {
                labelRepository.saveAll(all.iterator());//加入到缓存中去
            }
            return all;
        }
    }
    //删除池子标签
    public boolean delete(Integer id){
        //删除数据库中的数据
>>>>>>> 656d1e0fc23ec798a8dba36f017a70cd2371a127
        PoolLabel one = poolLabelDao.findOne(id);
        if(one==null){
            return false;
        }
        poolLabelDao.delete(id);
<<<<<<< HEAD
=======

        //删除缓存中的数据
        Object obj = labelRepository.findOne(id);
        if (obj != null) {
            labelRepository.delLabel(id);//在缓存中删除
        }

>>>>>>> 656d1e0fc23ec798a8dba36f017a70cd2371a127
        return true;
    }

    //根据标签id查询标签
    public PoolLabel getOne(Integer id){
        if(id==null){
            return null;
        }
<<<<<<< HEAD
        PoolLabel label = poolLabelDao.findOne(id);
=======
        PoolLabel label = null;
        label = labelRepository.findOne(id);//从缓存中查
        if (label == null ) {//从数据库中查
            label = poolLabelDao.findOne(id);
            labelRepository.saveLabel(label);//加入到缓存中
        }
>>>>>>> 656d1e0fc23ec798a8dba36f017a70cd2371a127
        return label;
    }
}
