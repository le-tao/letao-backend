package cn.edu.upc.letao.service;

import cn.edu.upc.letao.dao.MemberCrimeDao;
import cn.edu.upc.letao.entity.CommonMemberCrime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Created by 风飞扬 on 2017/6/2.
 */
@Service
@Transactional
public class MemberCrimeService {
    @Autowired
    private MemberCrimeDao memberCrimeDao;

    //查询所有的被惩罚用户
    public Object findAll(){
        return memberCrimeDao.findAll();
    }

    //根据   惩罚是否过期 查看用户
    public List<CommonMemberCrime> findByExpired(Integer expired){
        if(expired==null){
            return null;
        }
        return memberCrimeDao.findByExpired(expired);
    }

    //更新 被惩罚的信息
    public void update(CommonMemberCrime crime){
        memberCrimeDao.save(crime);
    }

    //findone
    public CommonMemberCrime findOne(Integer cid){
        if(cid==null){
            return null;
        }
        return memberCrimeDao.findOne(cid);
    }


}
