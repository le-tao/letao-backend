package cn.edu.upc.letao.service;

import cn.edu.upc.letao.dao.CommentUpOrDownDao;
import cn.edu.upc.letao.dao.CommonCommentDao;
import cn.edu.upc.letao.entity.CommentUpOrDown;
import cn.edu.upc.letao.entity.CommonComment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Created by 风飞扬 on 2017/4/20.
 * 这是评论的业务层
 */
@Service
@Transactional
public class CommentService {
    @Autowired
    private CommonCommentDao commonCommentDao;

    @Autowired
    private CommentUpOrDownDao commentUpOrDownDao;

    @Autowired
    private GoldService goldService;
    @Autowired
    private ExpService expService;


    //分页获取所有消息
    public List<CommonComment> getCmtPageable(int page, String name){
        page--;
        page = page*20;
        if(name==null){
            name = "";
        }
        return commonCommentDao.findPageable(page,name);
    }

    //所有消息数量
    public Long countCmt(String name){
        if(name == null){
            name = "";
        }
        return commonCommentDao.countPageable(name);
    }

    //根据消息id获取所有评论
    public List<CommonComment> getCommentsByMid(Integer mid){
        if(mid==null){
            return null;
        }
        return commonCommentDao.findByMid(mid);
    }

    //添加评论
    public boolean addComment(CommonComment commonComment){
        if(commonComment != null){
            commonCommentDao.save(commonComment);
            return true;
        }
        return false;
    }
    //删除评论
    public boolean deleteComment(int commentid){
        if(commonCommentDao.exists(commentid)){
            commonCommentDao.delete(commentid);
            return true;
        }
        return false;
    }

    //更新评论的赞的或者踩的数量，并且记录下该用户的该行为  返回true页面应该更新数据  false表示页面不要更新数据
    public boolean addUpOrDown(CommentUpOrDown commentUpOrDown){
        if(commentUpOrDown != null){
            CommentUpOrDown upOrDown = commentUpOrDownDao.findByUidAndCommentid(commentUpOrDown.getUid(), commentUpOrDown.getCommentid());
            CommonComment com = commonCommentDao.findByCommentid(commentUpOrDown.getCommentid());

            if(com!=null){
                if(upOrDown!=null){//之前已经赞过或者踩过
                    //更新消息的赞或者踩
                    if(commentUpOrDown.getMyflag()==upOrDown.getMyflag()){//和之前的赞与踩一样  不做操作
                        return false;
                    }else{//与之前的赞与踩不一样  如果1:踩-1 赞+1;如果0:踩+1 赞-1
                        if(commentUpOrDown.getMyflag()==1){//赞
                            com.setUpnumber(com.getUpnumber()+1);
                            com.setDownnumebr(com.getDownnumebr()-1);
                        }
                        if(commentUpOrDown.getMyflag()==0){//踩
                            com.setDownnumebr(com.getDownnumebr()+1);
                            com.setUpnumber(com.getUpnumber()-1);
                        }
                    }
                }else{//第一次赞或者踩
                    if(commentUpOrDown.getMyflag()==1){//赞
                        com.setUpnumber(com.getUpnumber()+1);
                    }
                    if(commentUpOrDown.getMyflag()==0){//踩
                        com.setDownnumebr(com.getDownnumebr()+1);
                    }
                }
                //更新消息
                commonCommentDao.save(com);
                //记录下该用户的行为
                if(upOrDown!=null){
                    commentUpOrDown.setId(upOrDown.getId());
                }
                //upOrDown.setMyflag(msgUpOrDown.getMyflag());
                commentUpOrDownDao.save(commentUpOrDown);
                return true;
            }
            return false;
        }
        return false;
    }
}
