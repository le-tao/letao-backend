package cn.edu.upc.letao.service;

import cn.edu.upc.letao.dao.GroupDao;
import cn.edu.upc.letao.entity.CommonMember;
import cn.edu.upc.letao.entity.CommonUsergroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Created by 风飞扬 on 2017/6/6.
 */
@Service
@Transactional
public class GroupService {
    @Autowired
    private GroupDao groupDao;

    @Autowired
    private LoginRecordService loginRecordService;

    public List<CommonUsergroup> getAllGroups(String allgroup){
        List<CommonUsergroup> groups = groupDao.getAllGroups();
        System.out.println(groups);
        return groups;
    }

    //根据 积分 找到合适的群组
    public CommonUsergroup getProperGroup(int exp){
        return groupDao.getProperGroup(exp, exp);
    }

    public boolean add(CommonUsergroup ug){
        groupDao.save(ug);
        return true;
    }

    public boolean modify(CommonUsergroup ug){
        if(groupDao.findOne(ug.getGroupid()) == null){
            return false;
        }else{
            System.out.println("modify:"+ug.getGrouptitle()+" id:"+ug.getGroupid());
            groupDao.save(ug);
            return true;
        }
    }

    public boolean del(int id){
        if(groupDao.findOne(id) == null){
            return false;
        }else{
            groupDao.delete(id);
            return true;
        }
    }

    //用户每日登录金币增加
    public CommonMember addLoignGold(CommonMember member){
        if(member==null){
            return null;
        }
        //查找今日登录的记录
        boolean b = loginRecordService.isTodayLogin(member.getUid());
        if(!b){//如果今日没有登录
            CommonUsergroup group = groupDao.findOne(member.getGroupid());
            member.setGold(member.getGold()+group.getDailygold());
        }
        return member;
    }

    //发帖获得金币
    public CommonMember addIngold(CommonMember member){
        if(member==null){
            return null;
        }
        CommonUsergroup group = groupDao.findOne(member.getGroupid());
        member.setGold(member.getGold()+group.getIngold());
        return member;
    }
    //淘消息 减少金币
    public CommonMember delOutgold(CommonMember member){
        if(member==null){
            return null;
        }
        CommonUsergroup group = groupDao.findOne(member.getGroupid());
        member.setGold(member.getGold()-group.getOutgold());
        if(member.getGold()<0){
            member.setGold(0);
        }
        return member;
    }
}
