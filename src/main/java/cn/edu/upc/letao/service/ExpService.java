package cn.edu.upc.letao.service;

import cn.edu.upc.letao.entity.CommonMember;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by 风飞扬 on 2017/6/9.
 */
@Service
@Transactional
public class ExpService {
    @Autowired
    private BasicSettingService basicSettingService;

    //给发消息用户 增加经验值
    public CommonMember addPostExp(CommonMember member){
        if(member == null){
            return null;
        }
        member.setExp(member.getExp()+basicSettingService.getCurrentSetting().getPostexp());
        return member;
    }

    //增加用户互动的经验值
    public CommonMember addGetExp(CommonMember member){
        if(member == null){
            return null;
        }
        member.setExp(member.getExp()+basicSettingService.getCurrentSetting().getGetexp());
        return member;
    }


}
