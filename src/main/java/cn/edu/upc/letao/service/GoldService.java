package cn.edu.upc.letao.service;

import cn.edu.upc.letao.dao.CommonMemberDao;
import cn.edu.upc.letao.dao.GroupDao;
import cn.edu.upc.letao.entity.CommonMember;
import cn.edu.upc.letao.entity.CommonUsergroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by 风飞扬 on 2017/6/9.
 */
@Service
@Transactional
public class GoldService {

    @Autowired
    private GroupDao groupDao;

    @Autowired
    private LoginRecordService loginRecordService;

    @Autowired
    private CommonMemberDao commonMemberDao;


    //用户每日登录金币增加
    public CommonMember addLoignGold(CommonMember member){
        if(member==null){
            return null;
        }
        //查找今日登录的记录
        boolean b = loginRecordService.isTodayLogin(member.getUid());
        if(!b){//如果今日没有登录
            CommonUsergroup group = groupDao.findOne(member.getGroupid());
            member.setGold(member.getGold()+group.getDailygold());
        }
        return member;
    }

    //发帖获得金币
    public CommonMember addIngold(CommonMember member){
        if(member==null){
            return null;
        }
        CommonUsergroup group = groupDao.findOne(member.getGroupid());
        member.setGold(member.getGold()+group.getIngold());
        return member;
    }
    //淘消息 减少金币
    public CommonMember delOutgold(CommonMember member){
        if(member==null){
            return null;
        }
        int goldTemp = member.getGold();
        CommonUsergroup group = groupDao.findOne(member.getGroupid());
        member.setGold(member.getGold()-group.getOutgold());
        if(member.getGold()<0){
            //如果金币不够不能淘消息的话 需要把金币恢复到原来的数量
            member.setGold(goldTemp);
        }
        return member;
    }

    //是否有足够的金币 淘消息
    public boolean isHaveGold(CommonMember member){
        if(member == null){
            return false;//用户为空  不允许淘消息
        }
        CommonUsergroup group = groupDao.findOne(member.getGroupid());
        if(member.getGold() >= group.getOutgold()){
            return true;//积分够了 可以淘消息
        }else {
            return false;//积分不够 不允许淘消息
        }
    }
    //是否有足够的金币 淘消息  函数重载
    public boolean isHaveGold(Integer uid){
        if(uid == null){
            return false;//用户为空  不允许淘消息
        }
        CommonMember member = commonMemberDao.findOne(uid);

        return isHaveGold(member);//调用楼上的方法
    }
}
