package cn.edu.upc.letao.service;

import cn.edu.upc.letao.dao.BasicSettingDao;
import cn.edu.upc.letao.entity.BasicSetting;
import cn.edu.upc.letao.entity.CommonMember;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by 风飞扬 on 2017/6/3.
 */
@Service
@Transactional
public class BasicSettingService {
    @Autowired
    private BasicSettingDao basicSettingDao;

    @Autowired
    private MemberService memberService;

    //添加或者是更新一个设置
    public void update(BasicSetting bs){
        if(bs != null){
            basicSettingDao.save(bs);
        }
    }
    //查询一个设置 根据id
    public BasicSetting findOne(Integer id){
        return basicSettingDao.findOne(id);
    }
    //删除一个设置
    public boolean delete(Integer id){
        BasicSetting bs = basicSettingDao.findOne(id);
        if(bs == null){
            return false;
        }
        basicSettingDao.delete(id);
        return true;
    }

    //获取id最大的设置(一般情况的意思是最新的设置)
    public BasicSetting findLatest(){
        int maxid = basicSettingDao.getMaxId();
        BasicSetting bs = basicSettingDao.findOne(maxid);
        return bs;
    }

    @Cacheable(value = "basic_setting")
    public BasicSetting getOnlySetting(Integer id){
        if(id==null){
            return null;
        }
        System.out.println("--这里对基础设置做了内存缓存--");
        return basicSettingDao.findOne(id);
    }

    /**
     * 获取当前的设置
     * @return
     */
    public BasicSetting getCurrentSetting(){
        return basicSettingDao.findOne(1);
    }

    //给注册用户设置 初始金币
    public CommonMember initRegisterMember(CommonMember member){
        if(member == null){
            return null;
        }
        member.setGold(getCurrentSetting().getInitgold());
        member.setExp(0);//经验值为 0
        return member;
    }

    //给发消息用户 增加经验值
    public CommonMember addPostExp(CommonMember member){
        if(member == null){
            return null;
        }
        member.setExp(member.getExp()+getCurrentSetting().getPostexp());
        return member;
    }
    //增加用户互动的经验值
    public CommonMember addGetExp(CommonMember member){
        if(member == null){
            return null;
        }
        member.setExp(member.getExp()+getCurrentSetting().getGetexp());
        return member;
    }


}
