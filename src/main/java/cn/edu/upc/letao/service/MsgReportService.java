package cn.edu.upc.letao.service;

import cn.edu.upc.letao.dao.CommonMessageDao;
import cn.edu.upc.letao.dao.MsgReportDao;
import cn.edu.upc.letao.entity.CommonMessage;
import cn.edu.upc.letao.entity.CommonReport;
import cn.edu.upc.letao.util.MyTimeTransfer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by 风飞扬 on 2017/6/7.
 */
@Service
@Transactional
public class MsgReportService {
    @Autowired
    private MsgReportDao msgReportDao;
    @Autowired
    private CommonMessageDao commonMessageDao;

    //分页获取所有消息
    public List<CommonReport> getPageable(int page){
        page--;
        page = page*20;
        return msgReportDao.findPageable(page);
    }

    //所有消息数量
    public Long count(){
        return msgReportDao.countPageable();
    }

    //处理举报信息违法
    public boolean check(int id,int op){
        CommonReport commonReport = msgReportDao.findOne(id);
        if(op == 1){
            commonReport.setOpresult(1);
        }else if(op == 2){
            commonReport.setOpresult(2);
            commonMessageDao.delete(commonReport.getMid());
        }
        msgReportDao.save(commonReport);
        return true;
    }

    //判断 某用户是否举报过某条消息
    //true表示举报过  false表示没有举报过
    public boolean isReport(Integer uid, Integer mid){
        if(uid == null || mid==null){
            return false;//参数为空 没有举报
        }
        CommonReport cr = msgReportDao.findByMidAndUid(mid, uid);
        if(cr == null ){
            return false;//没有举报
        } else {
            return true;//举报了
        }
    }

    //举报某条消息
    public boolean reportMsg(Integer uid, Integer mid){
        if(uid == null || mid == null){
            return false;//参数为空了 举报失败
        }
        boolean isreport = isReport(uid, mid);
        if(isreport){
            return false;//已经举报过了 再次举报失败
        }
        CommonReport cr = new CommonReport();
        cr.setUid(uid);
        cr.setMid(mid);
        cr.setOpresult(3);
        cr.setDate(MyTimeTransfer.getCurrentTimeStamp());

        msgReportDao.save(cr);
        return true;//举报成功
    }

    //取消举报某条消息
    public boolean cancleReportMsg(Integer uid, Integer mid) {
        if(uid == null || mid == null){
            return false;//参数为空 取消举报失败
        }
        boolean isreport = isReport(uid, mid);
        if(!isreport){
            return false;//没有举报过   取消举报失败
        }
        CommonReport cr = msgReportDao.findByMidAndUid(mid, uid);
        msgReportDao.delete(cr);
        return true;//取消举报成功
    }
}
