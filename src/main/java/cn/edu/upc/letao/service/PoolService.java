package cn.edu.upc.letao.service;

import cn.edu.upc.letao.dao.CollectionPoolDao;
import cn.edu.upc.letao.dao.CommonMemberDao;
import cn.edu.upc.letao.dao.CommonPoolDao;
import cn.edu.upc.letao.entity.CommonCollectionPool;
import cn.edu.upc.letao.entity.CommonMember;
import cn.edu.upc.letao.entity.CommonPool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 风飞扬 on 2017/4/6.
 * 这是池子(Pool)的业务层
 */
@Service
@Transactional
public class PoolService {
    @Autowired
    private CommonPoolDao commonPoolDao;

    @Autowired
    private CommonMemberDao commonMemberDao;

    @Autowired
    private CollectionPoolDao collectionPoolDao;

    //保存池子
    public boolean savePool(CommonPool pool) {
        if (pool != null) {
            commonPoolDao.save(pool);
            return true;
        }
        else
            return false;
    }

    //删除池子
    public boolean deletePool(CommonPool pool){
        if(pool!=null){
            commonPoolDao.delete(pool);
            return true;
        }
        else
            return false;
    }

    //根据id删除池子
    public boolean deleteOne(int id){
        System.out.println(id);
        commonPoolDao.delete(id);
        return true;
    }

    //获取所有池子
    public Object getAll(){
        return commonPoolDao.findAll();
    }

    //获取某一页的池子
    public Object getPageable(int page, String name){
        page--;
        page = page * 20;
        return commonPoolDao.findPagePools(page,name);
    }

    //池子总数
    public Long poolNum(String name){
        return commonPoolDao.countPools(name);
    }

    //根据池子标签来查询池子
    public List<CommonPool> findPoolsByLableid(Integer lableid){
        if(lableid==null){
            return null;
        }
        return commonPoolDao.findByLabelid(lableid);
    }

    //关注池子
    public boolean focusPool(Integer poolid, Integer uid){
        if(poolid==null || uid==null){
            return false;
        }
        CommonPool pool = commonPoolDao.findOne(poolid);
        //CommonPool user = commonPoolDao.findOne(uid);
        CommonMember user = commonMemberDao.findOne(uid);
        if(pool==null || user==null){
            return false;
        }
        //添加一条用户与池子关联的记录
        CommonCollectionPool poolNew = new CommonCollectionPool();
        poolNew.setPoolid(poolid);
        poolNew.setUid(uid);
        collectionPoolDao.save(poolNew);

        //池子关注人数增加1
        pool.setFocusnum(pool.getFocusnum()+1);
        commonPoolDao.save(pool);
        return true;
    }
    //取消关注池子
    public boolean cancelFocusPool(Integer poolid, Integer uid){
        if(poolid==null || uid==null){
            return false;
        }
        CommonPool pool = commonPoolDao.findOne(poolid);
        //CommonPool user = commonPoolDao.findOne(uid);
        CommonMember user = commonMemberDao.findOne(uid);
        if(pool==null || user==null){
            return false;
        }

        //找到收藏池子的记录
        CommonCollectionPool collePool = collectionPoolDao.findByPoolidAndUid(poolid, uid);
        collectionPoolDao.delete(collePool);

        //池子关注人数-1
        pool.setFocusnum(pool.getFocusnum()-1);
        commonPoolDao.save(pool);

        return true;
    }
    //推荐池子
    public List<CommonPool> recommendPool(Integer uid,Integer labelid){
        if(uid==null || labelid==null){
            return null;
        }
        List<CommonPool> pools = findPoolsByLableid(labelid);
        List<CommonCollectionPool> collecPoolsCollec = collectionPoolDao.findByUid(uid);
        List<CommonPool> collecPools = new ArrayList<CommonPool>();
        CommonPool c = null;
        if(collecPoolsCollec!=null){
            for (CommonCollectionPool co:collecPoolsCollec) {
                c = commonPoolDao.findOne(co.getPoolid());
                if(c!=null){
                    collecPools.add(c);
                }

            }
        }

        if(pools!=null){
            if(collecPools!=null){
                pools.removeAll(collecPools);
            }
        }
        return pools;
    }
    //判断是否已经关注了池子
    public boolean isFocus(Integer uid,Integer poolid){
        if(uid==null || poolid==null){
            return false;
        }
        CommonCollectionPool ccp = collectionPoolDao.findByPoolidAndUid(poolid, uid);
        if(ccp != null){
            return true;
        }
        return false;
    }
    //查top的池子
    @Cacheable(value = "top5Pools")
    public List<CommonPool> getTop5Pools(String top5pools){
        System.out.println("******查询top5******");
        System.out.println("******查询top5******");
        return  commonPoolDao.findTop5Pools();
    }

    //查询用户 已经关注的池子
    public List<CommonPool> getAllFocusPools(Integer uid){
        if(uid == null){
            return null;
        }
        List<CommonCollectionPool> ccp = collectionPoolDao.findByUid(uid);
        List<CommonPool> poolList = new ArrayList<CommonPool>();
        if(ccp != null){
            for(CommonCollectionPool temp:ccp){
                CommonPool pool = commonPoolDao.findOne(temp.getPoolid());
                poolList.add(pool);
            }
        }
        return poolList;
    }

    //findone
    public CommonPool findOne(Integer id){
        if (id == null){
            return null;
        }
        return commonPoolDao.findOne(id);
    }
}
