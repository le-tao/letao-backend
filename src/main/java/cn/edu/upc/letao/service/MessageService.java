package cn.edu.upc.letao.service;

import cn.edu.upc.letao.dao.CollectionMsgDao;
import cn.edu.upc.letao.dao.CommonMemberDao;
import cn.edu.upc.letao.dao.CommonMessageDao;
import cn.edu.upc.letao.dao.CommonMsgUpOrDownDao;
import cn.edu.upc.letao.entity.*;
import cn.edu.upc.letao.entity.custom.CommonMessageCustom;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by 风飞扬 on 2017/4/5.
 * 消息(Message)的业务类
 */
@Service
@Transactional
public class MessageService {
    @Autowired
    private CommonMessageDao commonMessageDao;

    @Autowired
    private CommonMsgUpOrDownDao commonMsgUpOrDownDao;

    @Autowired
    private CollectionMsgDao collectionMsgDao;

    @Autowired
    private CommonMemberDao commonMemberDao;

    @Autowired
    private BasicSettingService basicSettingService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private PoolService poolService;
    @Autowired
    private MemberService memberService;
    @Autowired
    private GoldService goldService;
    @Autowired
    private ExpService expService;

    //分页获取所有消息
    public List<CommonMessage> getMsgPageable(int page, String name){
        page--;
        page = page*20;
        if(name==null){
            name = "";
        }
        return commonMessageDao.findPageable(page,name);
    }

    //所有消息数量
    public Long countMsg(String name){
        if(name == null){
            name = "";
        }
        return commonMessageDao.countPageable(name);
    }


    //根据消息id查消息
    public CommonMessage getMsgById(Integer mid){
        if(mid==null){
            return null;
        }
        return commonMessageDao.findByMid(mid);
    }

    //根据用户uid和池子id得到一条消息
    public CommonMessageCustom queryOneMessage(int poolid){

        List<Integer> idList = commonMessageDao.getMessageIdsByPoolid(poolid);
        int ranNum = new Random().nextInt(idList.size());
        CommonMessage message = commonMessageDao.findByMid(idList.get(ranNum));
        CommonMessageCustom messageCustom = new CommonMessageCustom();
        BeanUtils.copyProperties(message,messageCustom);
        return messageCustom;

    }

    //根据用户uid和池子id得到一条消息 必须是登录或者注册用户
    public CommonMessageCustom queryOneMessageByUidAndPoolId(int poolid, int uid){

        List<Integer> idList = commonMessageDao.getMessageIdsByPoolid(poolid);
        if(idList==null){
            return null;
        }
        int ranNum = new Random().nextInt(idList.size());
        CommonMessage message = commonMessageDao.findByMid(idList.get(ranNum));
        CommonMessageCustom messageCustom = new CommonMessageCustom();
        BeanUtils.copyProperties(message,messageCustom);

        boolean b = goldService.isHaveGold(uid);
        if(b){//金币够了
            messageCustom.setHasgold(1);

            //找到这个人
            CommonMember user = commonMemberDao.findOne(uid);
            //减少用户的金币
            user = goldService.delOutgold(user);
            //增加用户的经验值
            user = expService.addGetExp(user);
            //对修改过的 用户 持久化
            memberService.saveUserAndGroupid(user);
        }else{//金币不够
            messageCustom.setHasgold(0);
        }
        return messageCustom;
    }


    //save或者update一条消息
    public void saveMessage(CommonMessage message, Integer uid){
        if(message!=null){
            //保存消息
            commonMessageDao.save(message);

            //找到这个人
            CommonMember user = commonMemberDao.findOne(uid);
            //增加金币
            user = goldService.addIngold(user);
            //增加经验值
            user = expService.addPostExp(user);
            //保存修改过的user
            memberService.saveUserAndGroupid(user);
        }
    }


    //删除消息
    public boolean deleteMsg(int id){
        if(commonMessageDao.exists(id)){
            commonMessageDao.delete(id);
            return true;
        }
        return false;
    }

    //更新消息的赞的或者踩的数量，并且记录下该用户的该行为  返回true页面应该更新数据  false表示页面不要更新数据
    public boolean addUpOrDown(CommonMsgUpOrDown msgUpOrDown){
        if(msgUpOrDown != null){
            CommonMsgUpOrDown upOrDown = commonMsgUpOrDownDao.findByUidAndMid(msgUpOrDown.getUid(), msgUpOrDown.getMid());
            CommonMessage msg = commonMessageDao.findByMid(msgUpOrDown.getMid());

            if(msg!=null){
                if(upOrDown!=null){//之前已经赞过或者踩过
                    //更新消息的赞或者踩
                    if(msgUpOrDown.getMyflag()==upOrDown.getMyflag()){//和之前的赞与踩一样  不做操作
                        return false;
                    }else{//与之前的赞与踩不一样  如果1:踩-1 赞+1;如果0:踩+1 赞-1
                        if(msgUpOrDown.getMyflag()==1){//赞
                            msg.setUpnum(msg.getUpnum()+1);
                            msg.setDownnum(msg.getDownnum()-1);
                        }
                        if(msgUpOrDown.getMyflag()==0){//踩
                            msg.setDownnum(msg.getDownnum()+1);
                            msg.setUpnum(msg.getUpnum()-1);
                        }
                    }
                }else{//第一次赞或者踩
                    if(msgUpOrDown.getMyflag()==1){//赞
                        msg.setUpnum(msg.getUpnum()+1);
                    }
                    if(msgUpOrDown.getMyflag()==0){//踩
                        msg.setDownnum(msg.getDownnum()+1);
                    }
                }
                //更新消息
                commonMessageDao.save(msg);
                //记录下该用户的行为
                if(upOrDown!=null){
                    msgUpOrDown.setId(upOrDown.getId());
                }
                //upOrDown.setMyflag(msgUpOrDown.getMyflag());
                commonMsgUpOrDownDao.save(msgUpOrDown);
                return true;
            }
            return false;
        }
        return false;
    }


    //判断是否收藏了某条消息
    public boolean isCollect(Integer uid, Integer mid){
        if(uid==null || mid==null){
            return false;
        }
        CommonCollectionMessage ccm = collectionMsgDao.findByMidAndUid(mid, uid);
        if(ccm != null){
            return true;
        }
        return false;
    }
    //推荐消息 top10消息
    public List<CommonMessage> recommendTop10Msgs(Integer poolid){
        if(poolid == null){
            return null;
        }
        List<CommonMessage> top10Msgs = commonMessageDao.findTop10Msgs(poolid);
        return top10Msgs;
    }

    //判断用户是否已经赞过某条消息
    //-1表示没有赞或者踩 0表示已经踩过， 1表示已经赞过
    public int isUpOrDown(Integer mid, Integer uid){
        if(mid==null || uid==null){
            return -1;
        }

        CommonMsgUpOrDown cmud = commonMsgUpOrDownDao.findByUidAndMid(uid, mid);
        if(cmud==null){
            return -1;
        }else{
            if(cmud.getMyflag()==1){
                return 1;
            }else{
                return 0;
            }
        }
    }

    //收藏某条消息
    public boolean collecMsg(Integer uid, Integer mid){
        if(uid==null || mid==null){
            return false;
        }
        CommonMessage msg = commonMessageDao.findOne(mid);
        CommonMember user = commonMemberDao.findOne(uid);
        if(msg==null || msg==null){
            return false;
        }
        //添加一条用户与消息 收藏关系的记录
        CommonCollectionMessage ccm = new CommonCollectionMessage();
        ccm.setUid(uid);
        ccm.setMid(mid);
        collectionMsgDao.save(ccm);

        //消息收藏人数+1
        msg.setCollectionnum(msg.getCollectionnum()+1);
        commonMessageDao.save(msg);
        return true;
    }

    //取消收藏某条消息
    public boolean cancelCollectMsg(Integer mid, Integer uid){
        if(mid==null || uid==null){
            return false;
        }
        CommonMessage msg = commonMessageDao.findOne(mid);
        CommonMember user = commonMemberDao.findOne(uid);
        if(msg==null || user==null){
            return false;
        }
        //找到收藏消息的记录
        CommonCollectionMessage ccm = collectionMsgDao.findByMidAndUid(mid, uid);
        collectionMsgDao.delete(ccm);

        //消息收藏人数-1
        msg.setCollectionnum(msg.getCollectionnum()-1);
        commonMessageDao.save(msg);

        return true;
    }

    //查询全局top5的消息
    @Cacheable(value = "top5Msgs")
    public List<CommonMessage> getTop5Msgs(String top5Msgs){
        System.out.println("******查询top5******");
        System.out.println("******查询top5******");
        return commonMessageDao.findTop5Msgs();

    }
    //查询用户 已经收藏的所有消息
    public List<CommonMessageCustom> getAllCollectMsgs(Integer uid){
        if(uid == null){
            return null;
        }
        List<CommonCollectionMessage> ccm = collectionMsgDao.findByUid(uid);
        List<CommonMessageCustom> msgList = new ArrayList<CommonMessageCustom>();

        if(ccm != null){
            for(CommonCollectionMessage temp:ccm){
                CommonMessage message = commonMessageDao.findByMid(temp.getMid());
                CommonMessageCustom cmc = new CommonMessageCustom();
                BeanUtils.copyProperties(message, cmc);
                CommonPool pool = poolService.findOne(temp.getMid());
                cmc.setLabelid(pool.getLabelid());

                if(cmc != null){

                    msgList.add(cmc);
                }
            }
        }
        return msgList;
    }
}