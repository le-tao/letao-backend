package cn.edu.upc.letao.redisRepository;

import cn.edu.upc.letao.entity.PoolLabel;

import java.util.Iterator;
import java.util.List;

/**
 * Created by 风飞扬 on 2017/6/26.
 */
public interface LabelRepository {

    public boolean saveLabel(PoolLabel label);//增或者改

    public boolean delLabel(Integer labelid);//删

    public Object findAllLabels();//查寻所有

    public PoolLabel findOne(Integer labelid);//查询单个

    public boolean saveAll(List<PoolLabel> labels);//批量保存

    public boolean saveAll(Iterator<PoolLabel> labels);//批量保存



}
