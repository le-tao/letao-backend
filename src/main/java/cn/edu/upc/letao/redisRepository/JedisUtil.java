package cn.edu.upc.letao.redisRepository;

import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * Created by 风飞扬 on 2017/6/26.
 */
@Component
public class JedisUtil {

    @Bean
    JedisConnectionFactory getJedisConnectionFactory(){
        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
        jedisConnectionFactory.setHostName("server0");//redis的ip地址
        //jedisConnectionFactory.setHostName("192.168.0.80");
        jedisConnectionFactory.setPort(6379);//redis的端口
        jedisConnectionFactory.setPassword("windfly");//访问redis的密码

        //jedisConnectionFactory.setTimeout(50000);

        System.out.println(jedisConnectionFactory);
        return jedisConnectionFactory;
    }
    @Bean
    public RedisTemplate<String, Object> getRedisTemplate() {
        RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
        template.setConnectionFactory(getJedisConnectionFactory());
        System.out.println(template);
        return template;
    }

}
