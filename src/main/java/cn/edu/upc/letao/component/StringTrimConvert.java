package cn.edu.upc.letao.component;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by 风飞扬 on 2017/4/10.
 * 这是一个转换器，将接收到页面的String去空格trim()操作(***去除半角状态下的空格***)
 */
@Component
public class StringTrimConvert implements Converter<String,String> {

    @Override
    public String convert(String source) {
        if(source != null){
            source = source.trim();
            if("".equals(source)){
                return null;
            }
            return source;
        }
        return null;
    }
}
