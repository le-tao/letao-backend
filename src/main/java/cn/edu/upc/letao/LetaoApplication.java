package cn.edu.upc.letao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class LetaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(LetaoApplication.class, args);
	}
}
