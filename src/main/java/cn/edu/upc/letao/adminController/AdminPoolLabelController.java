package cn.edu.upc.letao.adminController;

import cn.edu.upc.letao.entity.PoolLabel;
import cn.edu.upc.letao.model.ReturnMessage;
import cn.edu.upc.letao.service.PoolLabelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by 风飞扬 on 2017/6/2.
 */
@RestController
@RequestMapping("/admin/label")
public class AdminPoolLabelController {
    @Autowired
    private PoolLabelService poolLabelService;

    //查询所有的池子标签
    @RequestMapping(value = "findAll")
    public Object findAll(){
        return poolLabelService.findAll();
    }

    //添加一个或者是修改一个池子标签
    // (returnMessage id是1表示添加失败)
    @RequestMapping(value = "save")
    public Object save(PoolLabel pl){
        ReturnMessage returnMessage = new ReturnMessage();
        returnMessage.id = 0;
        returnMessage.message = "添加成功";
        if(pl==null){
            returnMessage.id = 1;
            returnMessage.message = "内容不能为空";
        }
        poolLabelService.add(pl);

        return returnMessage;

    }

    //删除一个标签
    //返回值1表示删除成功 1表示失败
    @RequestMapping(value = "delete")
    public Object delete(Integer id){
        ReturnMessage returnMessage = new ReturnMessage();
        boolean b = poolLabelService.delete(id);
        if(b){//删除成功
            returnMessage.id = 0;
            returnMessage.message = "删除成功";

        }else{//删除失败
            returnMessage.id = 1;
            returnMessage.message = "删除失败";

        }
        return returnMessage;
    }
}
