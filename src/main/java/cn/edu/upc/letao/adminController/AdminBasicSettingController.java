package cn.edu.upc.letao.adminController;

import cn.edu.upc.letao.entity.BasicSetting;
import cn.edu.upc.letao.model.ReturnMessage;
import cn.edu.upc.letao.service.BasicSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by 风飞扬 on 2017/6/3.
 */
@RestController
@RequestMapping("/admin/setting")
public class AdminBasicSettingController {
    @Autowired
    private BasicSettingService basicSettingService;

    //增加or修改一个配置
    @RequestMapping(value = "save")
    public Object save(BasicSetting bs){
        ReturnMessage returnMessage = new ReturnMessage();
        if(bs==null){
            returnMessage.id = 0;
            returnMessage.message = "保存失败";
        }
        basicSettingService.update(bs);
        returnMessage.id = 1;
        returnMessage.message = "保存成功";
        return returnMessage;
    }
    //根据id查询一个设置
    @RequestMapping(value = "findById")
    public Object findById(Integer id){
        return basicSettingService.findOne(id);
    }
    //查询最新的一个设置、
    @RequestMapping(value = "findLatest")
    public Object findLatest(){
        return basicSettingService.findLatest();
    }
    //删除一个设置
    @RequestMapping(value = "delete")
    public Object delete(Integer id){
        ReturnMessage returnMessage = new ReturnMessage();
        boolean b = basicSettingService.delete(id);
        if(b){
            returnMessage.id = 1;
            returnMessage.message = "删除成功";
        }
        returnMessage.id = 0;
        returnMessage.message = "删除失败";
        return returnMessage;
    }
}
