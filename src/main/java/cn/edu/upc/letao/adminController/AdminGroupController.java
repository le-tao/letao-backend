package cn.edu.upc.letao.adminController;

import cn.edu.upc.letao.entity.CommonUsergroup;
import cn.edu.upc.letao.model.ReturnMessage;
import cn.edu.upc.letao.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by cheney on 06/06/2017.
 */
@RestController
@RequestMapping("/admin/group")
public class AdminGroupController {

    @Autowired
    private GroupService groupService;

    @RequestMapping("/findall")
    public Object findAll(){
        ReturnMessage returnMessage = new ReturnMessage();
        returnMessage.id = 0;
        returnMessage.message = groupService.getAllGroups("");
        return returnMessage;
    }

    @RequestMapping("/add")
    public Object add(CommonUsergroup ug){
        ReturnMessage returnMessage = new ReturnMessage();
        if(ug.getGrouptitle()==null || ug.getCreditshigher()==0){
            returnMessage.id = 1;
            returnMessage.message = "头衔、积分上限不得为空";
        }else if(groupService.add(ug)){
            returnMessage.id = 0;
            returnMessage.message = "添加成功";
        }else{
            returnMessage.id = 1;
            returnMessage.message = "添加失败";
        }
        return returnMessage;
    }

    @RequestMapping("/modify")
    public Object modify(CommonUsergroup ug){
        ReturnMessage returnMessage = new ReturnMessage();
        if(ug.getGroupid()==0 || ug.getCreditshigher()==0 || ug.getGrouptitle()==null){
            returnMessage.id = 1;
            returnMessage.message = "组id、头衔、积分上限不得为空";
        }else if(groupService.modify(ug)){
            returnMessage.id = 0;
            returnMessage.message = "修改成功";
        }else{
            returnMessage.id = 1;
            returnMessage.message = "修改失败";
        }
        return returnMessage;
    }

    @RequestMapping("/del")
    public Object del(int id){
        ReturnMessage returnMessage = new ReturnMessage();
        if(groupService.del(id)){
            returnMessage.id = 0;
            returnMessage.message = "删除成功";
        }else{
            returnMessage.id = 1;
            returnMessage.message = "删除失败";
        }
        return returnMessage;
    }

}
