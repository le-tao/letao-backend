package cn.edu.upc.letao.adminController;

import cn.edu.upc.letao.entity.CommonMessage;
import cn.edu.upc.letao.entity.custom.CommonMessageCustom;
import cn.edu.upc.letao.entity.queryVo.CommonMessageQueryVo;
import cn.edu.upc.letao.entity.queryVo.CommonMsgUpOrDownQueryVo;
import cn.edu.upc.letao.model.ReturnMessage;
import cn.edu.upc.letao.service.*;
import cn.edu.upc.letao.util.MyFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by 风飞扬 on 2017/4/5.
 *
 * 这是有关消息Message的控制器
 */
@RestController
@RequestMapping("/admin/msg")
public class AdminMessageController {
    @Autowired
    private MessageService messageService;

    //注入文件上传方法对象
    @Autowired
    private MyFileUpload mu;

    @Autowired
    private MsgReportService msgReportService;
    @Autowired
    private GoldService goldService;
    @Autowired
    private MemberService memberService;
    @Autowired
    private PoolService poolService;


    @RequestMapping(value = "getall")
    public Object getAll(int page,String name){
        class ReturnMessage{
            public int id;
            public Object message;
            public Long total;
        }
        ReturnMessage returnMessage = new ReturnMessage();
        returnMessage.message = messageService.getMsgPageable(page,name);
        returnMessage.total = messageService.countMsg(name);
        return returnMessage;
    }

    @RequestMapping(value = "delete")
    public Object delete(int id){
        ReturnMessage returnMessage = new ReturnMessage();
        if(messageService.deleteMsg(id)){
            returnMessage.id = 0;
            returnMessage.message = "删除成功";
        }else{
            returnMessage.id = 1;
            returnMessage.message = "删除失败";
        }
        return returnMessage;
    }

    @RequestMapping(value = "batchdelete")
    public Object batchDelete(String ids){
        ReturnMessage returnMessage = new ReturnMessage();
        String[] dels = ids.split(",");
        for(int i=0;i<dels.length;i++){
            messageService.deleteMsg(Integer.valueOf(dels[i]));
        }
        returnMessage.id = 0;
        returnMessage.message = "批量删除成功";

        return returnMessage;
    }


    @RequestMapping(value = "getuncheck")
    public Object getUncheck(int page,String name){
        class ReturnMessage{
            public int id;
            public Object message;
            public Long total;
        }
        ReturnMessage returnMessage = new ReturnMessage();
        returnMessage.message = msgReportService.getPageable(page);
        returnMessage.total = msgReportService.count();
        return returnMessage;
    }

    //1正常 2非法
    @RequestMapping(value = "check")
    public Object check(int id,int op){
        ReturnMessage returnMessage = new ReturnMessage();
        if(msgReportService.check(id,op)){
            returnMessage.id = 0;
            returnMessage.message = "审核成功";
        }else{
            returnMessage.id = 1;
            returnMessage.message = "审核失败";
        }
        return returnMessage;
    }

    //非法
    @RequestMapping(value = "batchcheck")
    public Object batchCheck(String ids){
        ReturnMessage returnMessage = new ReturnMessage();
        String[] dels = ids.split(",");
        for(int i=0;i<dels.length;i++){
            msgReportService.check(Integer.valueOf(dels[i]), 2);
        }
        returnMessage.id = 0;
        returnMessage.message = "批量设置非法成功";

        return returnMessage;
    }


    //更新消息赞或者踩的数量
    //传参格式commonMsgUpOrDown.uid   commonMsgUpOrDown.mid  commonMsgUpOrDown.myflag
    @RequestMapping(value = "addUpOrDown")
    public ReturnMessage addUpOrDown(CommonMsgUpOrDownQueryVo queryVo){
        ReturnMessage returnMessage = new ReturnMessage();
        if(queryVo!=null){
            boolean b = messageService.addUpOrDown(queryVo.getCommonMsgUpOrDown());
            if(b){
                returnMessage.id = 1;//此时页面应该更新赞或者踩的数量
                returnMessage.message = "页面更新赞或者踩的数量";
            }else{
                returnMessage.id = 0;//此时页面不需要改变赞或者踩的数量
                returnMessage.message = "页面不需要更新赞或者踩的数量";
            }
        }
        return returnMessage;
    }

    //判断消息是否被收藏
    @RequestMapping(value = "isCollect")
    public Object isCollect(Integer uid, Integer mid){
        ReturnMessage returnMessage = new ReturnMessage();
        boolean b = messageService.isCollect(uid, mid);
        if(b) {
            returnMessage.id = 1;
            returnMessage.message = "已经收藏";
        }else{
            returnMessage.id = 0;
            returnMessage.message = "没有收藏";
        }
        return returnMessage;
    }
    //推荐 top10消息
    @RequestMapping(value = "recommendTop10Msgs")
    public Object recommendTop10Msgs(Integer poolid){
        List<CommonMessage> top10Msgs = messageService.recommendTop10Msgs(poolid);
        return top10Msgs;
    }

    //判断用户是否已经赞过某条消息
    //-1表示没有赞或者踩 0表示已经踩过， 1表示已经赞过
    @RequestMapping(value = "isUpOrDown")
    public Object isUpOrDown(Integer mid, Integer uid){
        ReturnMessage returnMessage = new ReturnMessage();
        int myflag = messageService.isUpOrDown(mid, uid);
        switch(myflag){
            case -1:
                returnMessage.id = -1;
                returnMessage.message = "没有赞或者踩";
                break;
            case 0:
                returnMessage.id = 0;
                returnMessage.message = "已经踩过";
                break;
            case 1:
                returnMessage.id = 1;
                returnMessage.message = "已经赞过";
                break;
        }
        return returnMessage;
    }

    //收藏某条消息
    @RequestMapping(value = "collectMsg")
    public Object collectMsg(Integer uid, Integer mid){
        ReturnMessage returnMessage = new ReturnMessage();
        boolean b = messageService.collecMsg(uid, mid);
        if(b){
            returnMessage.id = 1;
            returnMessage.message = "收藏成功";
        }else{
            returnMessage.id = 0;
            returnMessage.message = "收藏失败";
        }
        return returnMessage;
    }
    //取消收藏某条消息
    @RequestMapping(value = "cancelCollectMsg")
    public Object cancelCollectMsg(Integer uid, Integer mid){
        ReturnMessage returnMessage = new ReturnMessage();
        boolean b = messageService.cancelCollectMsg(mid, uid);
        if(b){
            returnMessage.id = 1;
            returnMessage.message = "取消收藏成功";
        }else{
            returnMessage.id = 0;
            returnMessage.message = "取消收藏失败";
        }
        return returnMessage;
    }

    //top5
    @RequestMapping(value = "getTop5Msgs")
    public Object getTop5Msgs(){
        String top5Msgs = "top5Msgs";
       return  messageService.getTop5Msgs(top5Msgs);
    }

    //查询用户  所有的 已经收藏的消息
    @RequestMapping(value = "getAllCollectMsgs")
    public Object getAllCollectMsgs(Integer uid){
       return  messageService.getAllCollectMsgs(uid);
    }

    //举报消息
    @RequestMapping(value = "reportMsg")
    public Object reportMsg(Integer uid, Integer mid){
        ReturnMessage returnMessage = new ReturnMessage();
        boolean b = msgReportService.reportMsg(uid, mid);
        if(b){
            returnMessage.id = 1;
            returnMessage.message = "举报成功!";
        }else{
            returnMessage.id = 0;
            returnMessage.message = "举报失败!";
        }
        return returnMessage;
    }
    //取消举报消息
    @RequestMapping(value = "cancleReportMsg")
    public Object cancleReportMsg(Integer uid,Integer mid){
        ReturnMessage returnMessage = new ReturnMessage();
        boolean b = msgReportService.cancleReportMsg(uid, mid);
        if(b){
            returnMessage.id = 1;
            returnMessage.message = "取消举报成功";
        }else{
            returnMessage.id = 0;
            returnMessage.message = "取消举报失败";
        }
        return returnMessage;
    }
    //判断用户是否举报过某条消息
    @RequestMapping(value = "isReportMsg")
    public Object isReportMsg(Integer uid, Integer mid){
        ReturnMessage returnMessage = new ReturnMessage();
        boolean b = msgReportService.isReport(uid, mid);
        if(b){
            returnMessage.id = 1;
            returnMessage.message = "已经举报过了";
        }else{
            returnMessage.id = 0;
            returnMessage.message = "没有举报过";
        }
        return returnMessage;
    }

    @RequestMapping(value = "isHasGold")
    public Object isHasGold(Integer poolid, Integer uid){
        ReturnMessage returnMessage = new ReturnMessage();
        boolean b = goldService.isHaveGold(uid);
        if(b){
            returnMessage.id = 1;
            returnMessage.message = "积分够了能淘消息";
        } else {
            returnMessage.id = 0;
            returnMessage.message = "积分不够不可以淘消息";
        }
        return returnMessage;
    }
}

