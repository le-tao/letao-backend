package cn.edu.upc.letao.adminController;

import cn.edu.upc.letao.entity.BasicSetting;
import cn.edu.upc.letao.entity.CommonMember;
import cn.edu.upc.letao.model.ReturnMessage;
import cn.edu.upc.letao.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.awt.color.CMMException;
import java.rmi.server.RMIClassLoader;
import java.util.List;

/**
 * Created by cheney on 06/06/2017.
 */
@RestController
@RequestMapping("/admin/user")
public class AdminMemberController {

    @Autowired
    private MemberService memberService;

    @RequestMapping("/getall")
    public Object getAll(Integer page,String name){
        class ReturnMessage{
            public int id;
            public Object message;
            public Long total;
        }
        ReturnMessage returnMessage = new ReturnMessage();
        returnMessage.id = 0;
        List<CommonMember> commonMembers = memberService.getAllMembers(page, name);
        returnMessage.message = commonMembers;
        returnMessage.total = memberService.getNum();
        return returnMessage;
    }

    /*
    添加用户     */
    @RequestMapping("/add")
    public Object add(CommonMember commonMember){
        ReturnMessage returnMessage = new ReturnMessage();
        if(commonMember.getEmail()==null || commonMember.getUsername()==null || commonMember.getPassword()==null) {
            returnMessage.id = 1;
            returnMessage.message = "Email、用户名不得为空";
        }else{
            memberService.addMember(commonMember);
            returnMessage.id = 0;
            returnMessage.message = "添加成功";
        }
        return returnMessage;
    }

    /*
    修改用户
     */
    @RequestMapping("/modify")
    public Object modify(CommonMember commonMember){
        ReturnMessage returnMessage = new ReturnMessage();
        if(commonMember.getEmail()==null || commonMember.getUsername()==null || commonMember.getUid()==0) {
            returnMessage.id = 1;
            returnMessage.message = "uid、Email、用户名不得为空";
        }else{
            if(memberService.modifyMember(commonMember)){
                returnMessage.id = 0;
                returnMessage.message = "修改成功";
            }else{
                returnMessage.id = 1;
                returnMessage.message = "请检查您的输入";
            }
        }
        return returnMessage;
    }

    @RequestMapping("del")
    public Object del(int uid){
        ReturnMessage returnMessage = new ReturnMessage();
        memberService.delete(uid);
        returnMessage.id = 0;
        returnMessage.message = "删除成功";
        return returnMessage;
    }

    @RequestMapping("batchdel")
    public Object batchDel(String ids){
        ReturnMessage returnMessage = new ReturnMessage();
        String[] dels = ids.split(",");
        for(int i=0;i<dels.length;i++){
            memberService.delete(Integer.valueOf(dels[i]));
        }
        returnMessage.id = 0;
        returnMessage.message = "批量删除成功";
        return returnMessage;
    }

    /*
    获得未审核用户
     */
    @RequestMapping("/getuncheck")
    public Object getUncheck(){
        class ReturnMessage{
            public int id;
            public Object message;
            public Long total;
        }
        ReturnMessage returnMessage = new ReturnMessage();
        returnMessage.message = memberService.getUncheck();
        returnMessage.total = memberService.getUncheckNum();
        returnMessage.id = 0;
        return returnMessage;
    }

    /*
    审核用户,0通过,1删除,3不通过
     */
    @RequestMapping("/check")
    public Object check(int id,int status){
        ReturnMessage returnMessage = new ReturnMessage();
        if(memberService.check(id,status)){
            returnMessage.id = 0;
            returnMessage.message = "审核成功";
        }else{
            returnMessage.id = 1;
            returnMessage.message = "审核失败";
        }
        return returnMessage;
    }

    /*
    批量审核用户通过
     */
    @RequestMapping("/batchcheck")
    public Object batchCheck(String ids){
        ReturnMessage returnMessage = new ReturnMessage();
        String[] dels = ids.split(",");
        for(int i=0;i<dels.length;i++){
            memberService.check(Integer.valueOf(dels[i]),0);
        }
        returnMessage.id = 0;
        returnMessage.message = "批量审核成功";
        return returnMessage;
    }

    /*
    添加管理员
     */
    @RequestMapping("/addadmin")
    public Object addAdmin(CommonMember cm){
        ReturnMessage returnMessage = new ReturnMessage();
        if(cm.getUid() != 0 || cm.getUsername()==null || cm.getEmail()==null || cm.getPassword()==null){
            returnMessage.id = 1;
            returnMessage.message = "请检查用户名和邮箱";
        }else if(memberService.addAdmin(cm)){
            returnMessage.id = 0;
            returnMessage.message = "添加成功";
        }else{
            returnMessage.id = 1;
            returnMessage.message = "添加失败";
        }
        return returnMessage;
    }

    /*
    所有管理员
     */
    @RequestMapping("/getadmin")
    public Object getAdmin(){
        ReturnMessage returnMessage = new ReturnMessage();
        returnMessage.message = memberService.getAdmin();
        returnMessage.id = 0;
        return returnMessage;
    }

    /*
    撤销管理员
     */
    @RequestMapping("/removeadmin")
    public Object removeAdmin(int id){
        ReturnMessage returnMessage = new ReturnMessage();
        if(memberService.removeAdmin(id)){
            returnMessage.id = 0;
            returnMessage.message = "撤销成功";
        }else{
            returnMessage.id = 1;
            returnMessage.message = "撤销失败";
        }
        return returnMessage;
    }

    /*
    修改管理员
     */
    @RequestMapping("/editadmin")
    public Object editAdmin(CommonMember cm){
        ReturnMessage returnMessage = new ReturnMessage();
        if(cm.getUid() == 0 || cm.getEmail()==null || cm.getUsername()==null){
            returnMessage.id = 1;
            returnMessage.message = "uid、Email和用户名不得为空";
        }else if(memberService.editAdmin(cm)){
            returnMessage.id = 0;
            returnMessage.message = "修改成功";
        }else{
            returnMessage.id = 1;
            returnMessage.message = "修改失败";
        }
        return returnMessage;
    }

}
