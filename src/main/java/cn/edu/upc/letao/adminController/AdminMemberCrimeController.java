package cn.edu.upc.letao.adminController;

import cn.edu.upc.letao.entity.CommonMemberCrime;
import cn.edu.upc.letao.service.MemberCrimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by 风飞扬 on 2017/6/2.
 * 被惩罚用户相关控制器
 */
@RestController
@RequestMapping("/admin/ban")
public class AdminMemberCrimeController {
    @Autowired
    private MemberCrimeService memberCrimeService;

    //返回所有的被惩罚用户 过期与不过期的
    @RequestMapping(value = "findAll")
    public Object findAll(){
        return memberCrimeService.findAll();
    }

    //根据 过期不过期 返回被惩罚用户
    @RequestMapping(value = "findByExpired")
    public Object findByExpired(Integer id){
        return memberCrimeService.findByExpired(id);
    }

    //更新 被惩罚的信息
    //强制性要传入原来的 该被惩罚对象的主键cid
    @RequestMapping(value = "update")
    public Object update(CommonMemberCrime crime,Integer cid){
        //CommonMemberCrime crimePrevious = memberCrimeService.findOne(cid);
        memberCrimeService.update(crime);
        return null;
    }
}
