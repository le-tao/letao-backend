package cn.edu.upc.letao.adminController;

import cn.edu.upc.letao.entity.CommonComment;
import cn.edu.upc.letao.entity.queryVo.CommentQueryVo;
import cn.edu.upc.letao.entity.queryVo.CommentUpOrDownQueryVo;
import cn.edu.upc.letao.model.ReturnMessage;
import cn.edu.upc.letao.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by 风飞扬 on 2017/4/25.
 */
@RestController
@RequestMapping("/admin/comment")
public class AdminCommentController {
    @Autowired
    private CommentService commentService;



    @RequestMapping(value = "getall")
    public Object getAll(int page,String name){
        class ReturnMessage{
            public int id;
            public Object message;
            public Long total;
        }
        ReturnMessage returnMessage = new ReturnMessage();
        returnMessage.message = commentService.getCmtPageable(page,name);
        returnMessage.total = commentService.countCmt(name);
        return returnMessage;
    }

    @RequestMapping(value = "delete")
    public Object delete(int id){
        ReturnMessage returnMessage = new ReturnMessage();
        if(commentService.deleteComment(id)){
            returnMessage.id = 0;
            returnMessage.message = "删除成功";
        }else{
            returnMessage.id = 1;
            returnMessage.message = "删除失败";
        }
        return returnMessage;
    }

    @RequestMapping(value = "batchdelete")
    public Object batchDelete(String ids){
        ReturnMessage returnMessage = new ReturnMessage();
        String[] dels = ids.split(",");
        for(int i=0;i<dels.length;i++){
            commentService.deleteComment(Integer.valueOf(dels[i]));
        }
        returnMessage.id = 0;
        returnMessage.message = "批量删除成功";

        return returnMessage;
    }

    //根据消息id查看所有评论
    @RequestMapping(value = "/getComments")
    public List<CommonComment> getComments(Integer mid){
        return commentService.getCommentsByMid(mid);
    }

    //添加评论
    @RequestMapping(value = "/addComment")
    public ReturnMessage addComment(CommentQueryVo queryVo){
        ReturnMessage returnMessage = new ReturnMessage();
        if(queryVo!=null&&queryVo.getCommonComment()!=null){
            boolean b = commentService.addComment(queryVo.getCommonComment());
            if(b){
                returnMessage.id = 0;
                returnMessage.message="操作成功";
            }else{
                returnMessage.id = 1;
                returnMessage.message="操作失败";
            }
        }else{
            returnMessage.id = 1;
            returnMessage.message="操作失败";
        }
        return returnMessage;
    }
    //删除评论
    @RequestMapping(value = "/delComment")
    public ReturnMessage delComment(int commentid){
        ReturnMessage returnMessage = new ReturnMessage();
        boolean b = commentService.deleteComment(commentid);
        if(b){
            returnMessage.id = 0;
            returnMessage.message = "操作成功";
        }else{
            returnMessage.id = 1;
            returnMessage.message = "操作失败";
        }
        return returnMessage;
    }

    //更新评论赞或者踩的数量
    //传参格式 commentUpOrDown.uid  commentUpOrDown.commentid commentUpOrDown.myflag
    @RequestMapping(value = "addUpOrDown")
    public ReturnMessage addUpOrDown(CommentUpOrDownQueryVo queryVo){
        ReturnMessage returnMessage = new ReturnMessage();
        if(queryVo!=null){
            boolean b = commentService.addUpOrDown(queryVo.getCommentUpOrDown());
            if(b){
                returnMessage.id = 1;//此时页面应该更新赞或者踩的数量
                returnMessage.message = "页面更新赞或者踩的数量";
            }else{
                returnMessage.id = 0;
                returnMessage.message = "页面不需要更新赞或者踩的数量";
            }
        }
        return returnMessage;
    }

}
