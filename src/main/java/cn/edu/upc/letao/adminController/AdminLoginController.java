package cn.edu.upc.letao.adminController;

import cn.edu.upc.letao.model.ReturnMessage;
import cn.edu.upc.letao.service.AuthService;
import cn.edu.upc.letao.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by cheney on 2017/4/6.
 */
@RestController
@RequestMapping("/admin/auth")
public class AdminLoginController {
    @Autowired
    private LoginService loginService;
    @Autowired
    private AuthService authService;

    @RequestMapping("login")
    public Object loginVerify(String credit, String password) throws IOException {
        class ReturnMessage{
            public int id;
            public Object message;
            public Object user;
        }
        ReturnMessage returnMessage = new ReturnMessage();
        if(loginService.adminLogin(credit,password)){
            //登录成功
            returnMessage.id = 0;
            returnMessage.message = "登录成功";
            returnMessage.user = authService.currentAdmin();
        }else{
            //登录失败
            returnMessage.id = 1;
            returnMessage.message = "登录失败，请检查邮箱和密码";
            returnMessage.user = "";
        }

        return returnMessage;
    }

    @RequestMapping("logout")
    public Object logout(){
        ReturnMessage returnMessage = new ReturnMessage();
        authService.invalidLogin();
        returnMessage.id = 0;
        returnMessage.message = "退出成功";
        return returnMessage;
    }
}
