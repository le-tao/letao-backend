package cn.edu.upc.letao.adminController;

import cn.edu.upc.letao.entity.CommonPool;
import cn.edu.upc.letao.entity.queryVo.CommonPoolQueryVo;
import cn.edu.upc.letao.model.ReturnMessage;
import cn.edu.upc.letao.service.AuthService;
import cn.edu.upc.letao.service.MemberService;
import cn.edu.upc.letao.service.PoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by 风飞扬 on 2017/4/6.
 * 这是池子（Pool）的控制器
 */
@RestController
@RequestMapping("/admin/pool")
public class AdminPoolController {

    @Autowired
    private PoolService poolService;
    @Autowired
    private AuthService authService;
    @Autowired
    private MemberService memberService;
    @Autowired
    private HttpSession httpSession;

    //保存池子
    //示范参数 ?commonPool.createdate=1&commonPool.name="basketball"&commonPool.type=1&commonPool.uid=1
    @RequestMapping(value = "save")
    public Object savePool(CommonPool commonPool) {
        ReturnMessage returnMessage = new ReturnMessage();
        if (commonPool.getName() != null) {
            poolService.savePool(commonPool);
            returnMessage.id = 0;
            returnMessage.message = "添加成功";
        } else {
            returnMessage.id = 1;
            returnMessage.message = "添加失败";
        }
        return returnMessage;
    }

    @RequestMapping(value = "delete")
    public Object deletePool(int id){
        ReturnMessage returnMessage = new ReturnMessage();
        poolService.deleteOne(id);
        returnMessage.id = 0;
        returnMessage.message = "删除成功";
        return returnMessage;
    }

    @RequestMapping(value = "batchdel")
    public Object batchDel(String ids){
        ReturnMessage returnMessage = new ReturnMessage();
        String[] dels = ids.split(",");
        for(int i=0;i<dels.length;i++){
            poolService.deleteOne(Integer.valueOf(dels[i]));
        }
        returnMessage.id = 0;
        returnMessage.message = "批量删除成功";
        return returnMessage;
    }

    @RequestMapping(value = "update")
    public Object updatePool(CommonPoolQueryVo poolQuery){
        ReturnMessage returnMessage = new ReturnMessage();
        int uid = (int) httpSession.getAttribute("visit_user_id");
        if (!authService.isAuthenticated()) {
            returnMessage.id = 2;
            returnMessage.message = "未认证";
        }
        else if (memberService.getAdminId(uid) == 2) {
            if (poolQuery != null && poolQuery.getCommonPool() != null & poolQuery.getCommonPool().getName() != null && poolQuery.getCommonPool().getUid() != 0) {
                poolService.savePool(poolQuery.getCommonPool());
                returnMessage.id = 0;
                returnMessage.message = "修改成功";
            } else {
                returnMessage.id = 1;
                returnMessage.message = "修改失败";
            }
        }
        return returnMessage;
    }

    //获取分页池子
    @RequestMapping("getall")
    public Object getAll(Integer page, String name){
        if(name == null){
            name = "";
        }
        class ReturnMessage{
            public int id;
            public Object message;
            public Long total;
        }
        ReturnMessage returnMessage = new ReturnMessage();
        returnMessage.id = 0;
        returnMessage.message = poolService.getPageable(page,name);
        returnMessage.total = poolService.poolNum(name);
        return returnMessage;
    }

    //根据池子标签查询池子
    @RequestMapping("getByLabel")
    public Object getByLabel(Integer labelid){
        List<CommonPool> pools = poolService.findPoolsByLableid(labelid);
        return pools;
    }
}
