package cn.edu.upc.letao.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by 风飞扬 on 2017/6/7.
 */
public class MyTimeTransfer {

    /**
     * 获取当前系统时间的时间戳（精确到秒）
     * @return int类型的时间戳
     */
    public static int getCurrentTimeStamp(){
        return (int)(System.currentTimeMillis()/1000);
    }

    /**
     * 将指定的时间戳转成 "yyyy-MM-dd HH:mm:ss"格式的字符串
     * @param stamp int类型的时间戳(精确到秒)
     * @return "yyyy-MM-dd HH:mm:ss"格式的字符串
     */
    public static String stampToStrtime(int stamp){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(new Long(stamp)*1000);
        String str = sdf.format(date);
        return str;
    }

    //获得当天0点时间戳
    public static int getTimesmorning(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return (int) (cal.getTimeInMillis()/1000);
    }
    //获得当天24点时间戳
    public static int getTimesnight(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 24);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return (int) (cal.getTimeInMillis()/1000);
    }

}
