package cn.edu.upc.letao.util;


import javax.mail.*;
import java.util.Properties;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
/**
 * Created by cheney on 2017/6/18.
 */
public class MailUtil {

    public static String HOST = "ali1.itmanbu.com";

    public static String PROTOCOL = "smtp";

    public static int PORT = 25;

    public static String FROM = "admin@smail.best.pm";//发件人的email

    public static String PWD = "YiPiao8072";//发件人密码

    /**
     * 获取Session
     *
     * @return
     */
    private static Session getSession() {
        Properties props = new Properties();
        props.put("mail.smtp.host", HOST);//设置服务器地址
        props.put("mail.store.protocol", PROTOCOL);//设置协议
        props.put("mail.smtp.port", PORT);//设置端口
        props.put("mail.smtp.auth", "true");

        Authenticator authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(FROM, PWD);
            }

        };
        Session session = Session.getDefaultInstance(props, authenticator);
//        session.setDebug(true);

        return session;
    }

    public static void send(String toEmail, String content) {
        Session session = getSession();
        try {
            System.out.println("--send--" + content);
            // Instantiate a message
            Message msg = new MimeMessage(session);

            //Set message attributes
            msg.setFrom(new InternetAddress(FROM));
            InternetAddress[] address = {new InternetAddress(toEmail)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject("邮件");
            msg.setSentDate(new Date());
            msg.setContent(content, "text/html;charset=utf-8");
            msg.saveChanges();

            //Send the message
            Transport.send(msg);
            System.out.println("发送完成");
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }
    }
}