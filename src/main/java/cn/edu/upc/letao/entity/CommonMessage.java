package cn.edu.upc.letao.entity;

import javax.persistence.*;

/**
 * Created by cheney on 2017/4/5.
 */
@Entity
@Table(name = "lt_common_message")
public class CommonMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int mid;

    private int uid;
    private int poolid;
    private int date;
    private String text;
    private String filepath;
    private int upnum;
    private int downnum;

    //新添加的属性  消息被收藏的人数
    private int collectionnum;

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getPoolid() {
        return poolid;
    }

    public void setPoolid(int poolid) {
        this.poolid = poolid;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    public int getUpnum() {
        return upnum;
    }

    public void setUpnum(int upnum) {
        this.upnum = upnum;
    }

    public int getDownnum() {
        return downnum;
    }

    public void setDownnum(int downnum) {
        this.downnum = downnum;
    }

    public CommonMessage(int uid, int poolid, int date, String text, String filepath, int upnum, int downnum) {

        this.uid = uid;
        this.poolid = poolid;
        this.date = date;
        this.text = text;
        this.filepath = filepath;
        this.upnum = upnum;
        this.downnum = downnum;
    }

    public CommonMessage() {

    }

    public int getCollectionnum() {
        return collectionnum;
    }

    public void setCollectionnum(int collectionnum) {
        this.collectionnum = collectionnum;
    }
}
