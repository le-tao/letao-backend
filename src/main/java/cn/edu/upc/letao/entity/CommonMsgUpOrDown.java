package cn.edu.upc.letao.entity;

import javax.persistence.*;

/**
 * Created by 风飞扬 on 2017/4/18.
 * 用来记录用户赞或者是踩了某个消息
 */
@Entity
@Table(name = "lt_common_msg_upordown")
public class CommonMsgUpOrDown {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int uid;//用户id
    private int mid;//消息id

    private int myflag;//1 代表赞  0代表踩

    public CommonMsgUpOrDown() {
    }

    public CommonMsgUpOrDown(int uid, int mid, int myflag) {
        this.uid = uid;
        this.mid = mid;
        this.myflag = myflag;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public int getMyflag() {
        return myflag;
    }

    public void setMyflag(int myflag) {
        this.myflag = myflag;
    }
}
