package cn.edu.upc.letao.entity;

import javax.persistence.*;

/**
 * Created by 风飞扬 on 2017/4/27.
 *
 */
@Entity
@Table(name = "lt_comment_upordown")
public class CommentUpOrDown {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int uid;//用户id
    private int commentid;//评论id

    private int myflag;//1代表赞  0代表踩

    public CommentUpOrDown() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getCommentid() {
        return commentid;
    }

    public void setCommentid(int commentid) {
        this.commentid = commentid;
    }

    public int getMyflag() {
        return myflag;
    }

    public void setMyflag(int myflag) {
        this.myflag = myflag;
    }
}
