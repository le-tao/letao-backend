package cn.edu.upc.letao.entity.queryVo;

import cn.edu.upc.letao.entity.CommonComment;

/**
 * Created by 风飞扬 on 2017/4/25.
 */
public class CommentQueryVo {
    private CommonComment commonComment;

    public CommonComment getCommonComment() {
        return commonComment;
    }

    public void setCommonComment(CommonComment commonComment) {
        this.commonComment = commonComment;
    }
}
