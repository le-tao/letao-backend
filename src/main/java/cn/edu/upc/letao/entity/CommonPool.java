package cn.edu.upc.letao.entity;

import javax.persistence.*;

/**
 * Created by cheney on 2017/4/5.
 */
@Entity
@Table(name = "lt_common_pool")
public class CommonPool {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int poolid;

    private int uid;
    private int type;//类型：0系统，1用户
    private String name;
    private int createdate;//创建时间
    private int focusnum;//关注人数

    //新添加属性  池子标签id
    private int labelid;

    //新添加的属性  池子的描述
    private String pooldesc;

    public int getPoolid() {
        return poolid;
    }

    public void setPoolid(int poolid) {
        this.poolid = poolid;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCreatedate() {
        return createdate;
    }

    public void setCreatedate(int createdate) {
        this.createdate = createdate;
    }

    public int getFocusnum() {
        return focusnum;
    }

    public void setFocusnum(int focusnum) {
        this.focusnum = focusnum;
    }

    public CommonPool(int uid, int type, String name, int createdate, int focusnum) {

        this.uid = uid;
        this.type = type;
        this.name = name;
        this.createdate = createdate;
        this.focusnum = focusnum;
    }

    public CommonPool() {

    }

    public int getLabelid() {
        return labelid;
    }

    public void setLabelid(int labelid) {
        this.labelid = labelid;
    }

    public String getPooldesc() {
        return pooldesc;
    }

    public void setPooldesc(String pooldesc) {
        this.pooldesc = pooldesc;
    }
}
