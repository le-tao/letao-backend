package cn.edu.upc.letao.entity;

import javax.persistence.*;

/**
 * Created by cheney on 2017/4/5.
 */
@Entity
@Table(name = "lt_common_report")
public class CommonReport {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int rid;

    private int uid;//举报人id
    private int mid;//消息id
    private int date;
    private int adminid;//消息处理人
    private int optime;
    private int opresult;//处理结果 3表示等待审核 2表示消息非法 1表示消息正常


    public int getRid() {
        return rid;
    }

    public void setRid(int rid) {
        this.rid = rid;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public int getAdminid() {
        return adminid;
    }

    public void setAdminid(int adminid) {
        this.adminid = adminid;
    }

    public int getOptime() {
        return optime;
    }

    public void setOptime(int optime) {
        this.optime = optime;
    }

    public int getOpresult() {
        return opresult;
    }

    public void setOpresult(int opresult) {
        this.opresult = opresult;
    }

    public CommonReport() {
    }
}
