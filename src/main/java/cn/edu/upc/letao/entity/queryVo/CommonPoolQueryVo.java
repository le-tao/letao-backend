package cn.edu.upc.letao.entity.queryVo;

import cn.edu.upc.letao.entity.CommonPool;

/**
 * Created by 风飞扬 on 2017/4/6.
 * 这是池子(Pool)的包装类
 */
public class CommonPoolQueryVo {

    private CommonPool commonPool;


    public CommonPool getCommonPool() {
        return commonPool;
    }

    public void setCommonPool(CommonPool commonPool) {
        this.commonPool = commonPool;
    }
}
