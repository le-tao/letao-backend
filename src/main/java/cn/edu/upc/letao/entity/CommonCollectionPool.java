package cn.edu.upc.letao.entity;

import javax.persistence.*;

/**
 * Created by cheney on 2017/4/5.
 */
@Entity
@Table(name = "lt_common_collection_pool")
public class CommonCollectionPool {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int uid;
    private int poolid;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getPoolid() {
        return poolid;
    }

    public void setPoolid(int poolid) {
        this.poolid = poolid;
    }

    public CommonCollectionPool() {
    }
}
