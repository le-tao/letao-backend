package cn.edu.upc.letao.entity.queryVo;

import cn.edu.upc.letao.entity.CommonMsgUpOrDown;

/**
 * Created by 风飞扬 on 2017/4/20.
 * 消息赞或者踩记录的包装类
 */
public class CommonMsgUpOrDownQueryVo {

    private CommonMsgUpOrDown commonMsgUpOrDown;

    public CommonMsgUpOrDown getCommonMsgUpOrDown() {
        return commonMsgUpOrDown;
    }

    public void setCommonMsgUpOrDown(CommonMsgUpOrDown commonMsgUpOrDown) {
        this.commonMsgUpOrDown = commonMsgUpOrDown;
    }
}
