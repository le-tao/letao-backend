package cn.edu.upc.letao.entity;

import javax.persistence.*;

/**
 * Created by cheney on 2017/4/5.
 */
@Entity
@Table(name = "lt_common_admingroup")
public class CommonAdmingroup {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int admingid;
    private int allowmodpost;//允许审核帖子
    private int allowdelpost;//允许删除帖子
    private int allowmassprune;//允许批量删帖
    private int allowviewip;//允许查看ip
    private int allowbanip;//允许禁止ip
    private int allowedituser;//允许审核用户
    private int allowmoduser;//允许审核用户
    private int allowbanuser;//允许禁止用户发言
    private int allowbanvisituser;//允许禁止用户访问
    private int allowpostannounce;//允许发布站点公告
    private int allowclosethread;//允许关闭主题
    private int allowmovethread;//允许移动主题
    private int allowedittypethread;//允许编辑主题分类
    private int allowmergethread;//允许合并主题
    private int allowsplitthread;//允许分割主题
    private int allowrepairthread;//允许修复主题
    private int allowwarnpost;//允许警告帖子
    private int managecomment;//允许管理评论
    private int managereport;//允许管理举报

    public int getAdmingid() {
        return admingid;
    }

    public void setAdmingid(int admingid) {
        this.admingid = admingid;
    }

    public int getAllowmodpost() {
        return allowmodpost;
    }

    public void setAllowmodpost(int allowmodpost) {
        this.allowmodpost = allowmodpost;
    }

    public int getAllowdelpost() {
        return allowdelpost;
    }

    public void setAllowdelpost(int allowdelpost) {
        this.allowdelpost = allowdelpost;
    }

    public int getAllowmassprune() {
        return allowmassprune;
    }

    public void setAllowmassprune(int allowmassprune) {
        this.allowmassprune = allowmassprune;
    }

    public int getAllowviewip() {
        return allowviewip;
    }

    public void setAllowviewip(int allowviewip) {
        this.allowviewip = allowviewip;
    }

    public int getAllowbanip() {
        return allowbanip;
    }

    public void setAllowbanip(int allowbanip) {
        this.allowbanip = allowbanip;
    }

    public int getAllowedituser() {
        return allowedituser;
    }

    public void setAllowedituser(int allowedituser) {
        this.allowedituser = allowedituser;
    }

    public int getAllowmoduser() {
        return allowmoduser;
    }

    public void setAllowmoduser(int allowmoduser) {
        this.allowmoduser = allowmoduser;
    }

    public int getAllowbanuser() {
        return allowbanuser;
    }

    public void setAllowbanuser(int allowbanuser) {
        this.allowbanuser = allowbanuser;
    }

    public int getAllowbanvisituser() {
        return allowbanvisituser;
    }

    public void setAllowbanvisituser(int allowbanvisituser) {
        this.allowbanvisituser = allowbanvisituser;
    }

    public int getAllowpostannounce() {
        return allowpostannounce;
    }

    public void setAllowpostannounce(int allowpostannounce) {
        this.allowpostannounce = allowpostannounce;
    }

    public int getAllowclosethread() {
        return allowclosethread;
    }

    public void setAllowclosethread(int allowclosethread) {
        this.allowclosethread = allowclosethread;
    }

    public int getAllowmovethread() {
        return allowmovethread;
    }

    public void setAllowmovethread(int allowmovethread) {
        this.allowmovethread = allowmovethread;
    }

    public int getAllowedittypethread() {
        return allowedittypethread;
    }

    public void setAllowedittypethread(int allowedittypethread) {
        this.allowedittypethread = allowedittypethread;
    }

    public int getAllowmergethread() {
        return allowmergethread;
    }

    public void setAllowmergethread(int allowmergethread) {
        this.allowmergethread = allowmergethread;
    }

    public int getAllowsplitthread() {
        return allowsplitthread;
    }

    public void setAllowsplitthread(int allowsplitthread) {
        this.allowsplitthread = allowsplitthread;
    }

    public int getAllowrepairthread() {
        return allowrepairthread;
    }

    public void setAllowrepairthread(int allowrepairthread) {
        this.allowrepairthread = allowrepairthread;
    }

    public int getAllowwarnpost() {
        return allowwarnpost;
    }

    public void setAllowwarnpost(int allowwarnpost) {
        this.allowwarnpost = allowwarnpost;
    }

    public int getManagecomment() {
        return managecomment;
    }

    public void setManagecomment(int managecomment) {
        this.managecomment = managecomment;
    }

    public int getManagereport() {
        return managereport;
    }

    public void setManagereport(int managereport) {
        this.managereport = managereport;
    }

    public CommonAdmingroup(int allowmodpost, int allowdelpost, int allowmassprune, int allowviewip, int allowbanip, int allowedituser, int allowmoduser, int allowbanuser, int allowbanvisituser, int allowpostannounce, int allowclosethread, int allowmovethread, int allowedittypethread, int allowmergethread, int allowsplitthread, int allowrepairthread, int allowwarnpost, int managecomment, int managereport) {

        this.allowmodpost = allowmodpost;
        this.allowdelpost = allowdelpost;
        this.allowmassprune = allowmassprune;
        this.allowviewip = allowviewip;
        this.allowbanip = allowbanip;
        this.allowedituser = allowedituser;
        this.allowmoduser = allowmoduser;
        this.allowbanuser = allowbanuser;
        this.allowbanvisituser = allowbanvisituser;
        this.allowpostannounce = allowpostannounce;
        this.allowclosethread = allowclosethread;
        this.allowmovethread = allowmovethread;
        this.allowedittypethread = allowedittypethread;
        this.allowmergethread = allowmergethread;
        this.allowsplitthread = allowsplitthread;
        this.allowrepairthread = allowrepairthread;
        this.allowwarnpost = allowwarnpost;
        this.managecomment = managecomment;
        this.managereport = managereport;
    }

    public CommonAdmingroup() {

    }
}
