package cn.edu.upc.letao.entity;

import javax.persistence.*;

/**
 * Created by cheney on 2017/4/5.
 */
@Entity
@Table(name="lt_common_member")
public class CommonMember {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int uid;

    private String email;
    private String username;
    private String password;
    private int avatarstatus;

    private int adminid;//
    private int groupid;
    private int regdate;
    private int exp;
    private int gold;
    //status是0正常 1删除 2禁发帖，
    // 开启注册审核以后注册完了是状态2，应该再加一个状态3表示审核失败
    private int status;
    private int emailstatus;

    public String getFilepath() {
        return filepath;
    }

    public void setFilepath(String filepath) {
        this.filepath = filepath;
    }

    private String filepath;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getEmailstatus() {
        return emailstatus;
    }

    public void setEmailstatus(int emailstatus) {
        this.emailstatus = emailstatus;
    }

    public int getAvatarstatus() {
        return avatarstatus;
    }

    public void setAvatarstatus(int avatarstatus) {
        this.avatarstatus = avatarstatus;
    }

    public int getAdminid() {
        return adminid;
    }

    public void setAdminid(int adminid) {
        this.adminid = adminid;
    }

    public int getGroupid() {
        return groupid;
    }

    public void setGroupid(int groupid) {
        this.groupid = groupid;
    }

    public int getRegdate() {
        return regdate;
    }

    public void setRegdate(int regdate) {
        this.regdate = regdate;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }


    public CommonMember() {
    }


    public CommonMember(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
