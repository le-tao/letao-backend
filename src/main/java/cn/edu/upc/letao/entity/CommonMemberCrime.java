package cn.edu.upc.letao.entity;

import javax.persistence.*;

/**
 * Created by cheney on 2017/4/5.
 */
@Entity
@Table(name = "lt_common_member_crime")
public class CommonMemberCrime {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int cid;

    private int uid;//被惩罚的用户uid
    private int operatorid;//惩罚者id
    private int action;//行为，0正常，1删除，2禁止发帖
    private String reason;//理由
    private int dateline;//操作时间
    private int duration;//持续时间，单位小时，0为长久有效
    private int expired;//是否已过期

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getOperatorid() {
        return operatorid;
    }

    public void setOperatorid(int operatorid) {
        this.operatorid = operatorid;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public int getDateline() {
        return dateline;
    }

    public void setDateline(int dateline) {
        this.dateline = dateline;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public int getExpired() {
        return expired;
    }

    public void setExpired(int expired) {
        this.expired = expired;
    }

    public CommonMemberCrime(int uid, int operatorid, int action, String reason, int dateline, int duration, int expired) {

        this.uid = uid;
        this.operatorid = operatorid;
        this.action = action;
        this.reason = reason;
        this.dateline = dateline;
        this.duration = duration;
        this.expired = expired;
    }

    public CommonMemberCrime() {

    }
}
