package cn.edu.upc.letao.entity;

import javax.persistence.*;

/**
 * Created by cheney on 2017/4/5.
 */
@Entity
@Table(name = "lt_common_comment")
public class CommonComment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int commentid;

    private int writerid;
    private int downnumebr;
    private String text;
    private int upnumber;
    private int time;
    private int mid;//消息id

    public int getMid() {
        return mid;
    }

    public void setMid(int mid) {
        this.mid = mid;
    }

    public int getCommentid() {
        return commentid;
    }

    public void setCommentid(int commentid) {
        this.commentid = commentid;
    }

    public int getWriterid() {
        return writerid;
    }

    public void setWriterid(int writerid) {
        this.writerid = writerid;
    }

    public int getDownnumebr() {
        return downnumebr;
    }

    public void setDownnumebr(int downnumebr) {
        this.downnumebr = downnumebr;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getUpnumber() {
        return upnumber;
    }

    public void setUpnumber(int upnumber) {
        this.upnumber = upnumber;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public CommonComment(int writerid, int downnumebr, String text, int upnumber, int time) {

        this.writerid = writerid;
        this.downnumebr = downnumebr;
        this.text = text;
        this.upnumber = upnumber;
        this.time = time;
    }

    public CommonComment() {

    }
}
