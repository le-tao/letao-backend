package cn.edu.upc.letao.entity.custom;

import cn.edu.upc.letao.entity.CommonMessage;

/**
 * Created by 风飞扬 on 2017/4/5.
 *
 * 这是消息(Message)类的扩展类
 */
public class CommonMessageCustom extends CommonMessage{
    private int hasgold;//金币是否够用

    private int labelid;//消息所在的池子，该池子所在标签

    public int getHasgold() {
        return hasgold;
    }

    public void setHasgold(int hasgold) {
        this.hasgold = hasgold;
    }

    public int getLabelid() {
        return labelid;
    }

    public void setLabelid(int labelid) {
        this.labelid = labelid;
    }
}
