package cn.edu.upc.letao.entity;

import javax.persistence.*;

/**
 * Created by cheney on 2017/4/5.
 */
@Entity
@Table(name = "lt_common_usergroup_permission")
public class CommonUsergroupPermission {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int groupid;
    private int allowpost;
    private int allowreply;
    private int allowcreatepool;
    private int allowcstatus;//允许自定义头衔
    private int allowreport;
    private int allowinvisible;
    private int allowanonymous;//匿名发帖
    private int allowsigimgcode;//允许使用img
    private int allowpoke;//允许打招呼
    private int allowfriend;//允许加好友

    public int getGroupid() {
        return groupid;
    }

    public void setGroupid(int groupid) {
        this.groupid = groupid;
    }

    public int getAllowpost() {
        return allowpost;
    }

    public void setAllowpost(int allowpost) {
        this.allowpost = allowpost;
    }

    public int getAllowreply() {
        return allowreply;
    }

    public void setAllowreply(int allowreply) {
        this.allowreply = allowreply;
    }

    public int getAllowcreatepool() {
        return allowcreatepool;
    }

    public void setAllowcreatepool(int allowcreatepool) {
        this.allowcreatepool = allowcreatepool;
    }

    public int getAllowcstatus() {
        return allowcstatus;
    }

    public void setAllowcstatus(int allowcstatus) {
        this.allowcstatus = allowcstatus;
    }

    public int getAllowreport() {
        return allowreport;
    }

    public void setAllowreport(int allowreport) {
        this.allowreport = allowreport;
    }

    public int getAllowinvisible() {
        return allowinvisible;
    }

    public void setAllowinvisible(int allowinvisible) {
        this.allowinvisible = allowinvisible;
    }

    public int getAllowanonymous() {
        return allowanonymous;
    }

    public void setAllowanonymous(int allowanonymous) {
        this.allowanonymous = allowanonymous;
    }

    public int getAllowsigimgcode() {
        return allowsigimgcode;
    }

    public void setAllowsigimgcode(int allowsigimgcode) {
        this.allowsigimgcode = allowsigimgcode;
    }

    public int getAllowpoke() {
        return allowpoke;
    }

    public void setAllowpoke(int allowpoke) {
        this.allowpoke = allowpoke;
    }

    public int getAllowfriend() {
        return allowfriend;
    }

    public void setAllowfriend(int allowfriend) {
        this.allowfriend = allowfriend;
    }

    public CommonUsergroupPermission(int allowpost, int allowreply, int allowcreatepool, int allowcstatus, int allowreport, int allowinvisible, int allowanonymous, int allowsigimgcode, int allowpoke, int allowfriend) {

        this.allowpost = allowpost;
        this.allowreply = allowreply;
        this.allowcreatepool = allowcreatepool;
        this.allowcstatus = allowcstatus;
        this.allowreport = allowreport;
        this.allowinvisible = allowinvisible;
        this.allowanonymous = allowanonymous;
        this.allowsigimgcode = allowsigimgcode;
        this.allowpoke = allowpoke;
        this.allowfriend = allowfriend;
    }

    public CommonUsergroupPermission() {

    }
}
