package cn.edu.upc.letao.entity.queryVo;

import cn.edu.upc.letao.entity.CommonMessage;

/**
 * Created by 风飞扬 on 2017/4/6.
 * 这是消息(Message)的包装类
 */
public class CommonMessageQueryVo {

    private CommonMessage commonMessage;

    public CommonMessage getCommonMessage() {
        return commonMessage;
    }

    public void setCommonMessage(CommonMessage commonMessage) {
        this.commonMessage = commonMessage;
    }
}
