package cn.edu.upc.letao.entity;

import javax.persistence.*;

/**
 * Created by 风飞扬 on 2017/6/2.
 * 这是新添加的池子标签类
 */
@Entity
@Table(name = "lt_common_pool_label")
public class PoolLabel {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int labelid;//池子标签id

    private String labelname;//池子标签名称

    public PoolLabel() {
    }

    public int getLabelid() {
        return labelid;
    }

    public void setLabelid(int labelid) {
        this.labelid = labelid;
    }

    public String getLabelname() {
        return labelname;
    }

    public void setLabelname(String labelname) {
        this.labelname = labelname;
    }
}
