package cn.edu.upc.letao.entity;

import javax.persistence.*;

/**
 * Created by 风飞扬 on 2017/6/3.
 * 系统设置
 */
@Entity
@Table(name = "lt_setting")
public class BasicSetting {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int postexp;//发一条消息增加的经验
    private int getexp;//其他互动增加的经验（淘消息、评论、点赞
    private int initgold;//初始金币

    private int ischeck;//新注册用户是否审核 1代表需要审核 0代表不需要审核

    private String sitename;//网站名
    private String siteurl;//网址
    private String mycopyright;//版权信息
    private String mykeywords;//meta信息中的keyword
    private String mydesc;//meta信息中的描述
    private int isoff;//1表示网站正常营业，0表示网站关闭
    private String offreason;//关闭网站原因

    public BasicSetting() {
    }

    public int getInitgold() {
        return initgold;
    }

    public void setInitgold(int initgold) {
        this.initgold = initgold;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSitename() {
        return sitename;
    }

    public void setSitename(String sitename) {
        this.sitename = sitename;
    }

    public String getSiteurl() {
        return siteurl;
    }

    public void setSiteurl(String siteurl) {
        this.siteurl = siteurl;
    }

    public String getMycopyright() {
        return mycopyright;
    }

    public void setMycopyright(String mycopyright) {
        this.mycopyright = mycopyright;
    }

    public String getMykeywords() {
        return mykeywords;
    }

    public void setMykeywords(String mykeywords) {
        this.mykeywords = mykeywords;
    }

    public String getMydesc() {
        return mydesc;
    }

    public int getPostexp() {
        return postexp;
    }

    public void setPostexp(int postexp) {
        this.postexp = postexp;
    }

    public int getGetexp() {
        return getexp;
    }

    public void setGetexp(int getexp) {
        this.getexp = getexp;
    }

    public void setMydesc(String mydesc) {
        this.mydesc = mydesc;
    }

    public int getIsoff() {
        return isoff;
    }

    public void setIsoff(int isoff) {
        this.isoff = isoff;
    }

    public String getOffreason() {
        return offreason;
    }

    public void setOffreason(String offreason) {
        this.offreason = offreason;
    }

    public int getIscheck() {
        return ischeck;
    }

    public void setIscheck(int ischeck) {
        this.ischeck = ischeck;
    }
}
