package cn.edu.upc.letao.entity;

import javax.persistence.*;

/**
 * Created by cheney on 2017/4/5.
 */
@Entity
@Table(name = "lt_common_notice")
public class CommonNotice {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int noticeid;

    private String name;
    private int time;
    private String text;

    public int getNoticeid() {
        return noticeid;
    }

    public void setNoticeid(int noticeid) {
        this.noticeid = noticeid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public CommonNotice(String name, int time, String text) {

        this.name = name;
        this.time = time;
        this.text = text;
    }

    public CommonNotice() {

    }
}
