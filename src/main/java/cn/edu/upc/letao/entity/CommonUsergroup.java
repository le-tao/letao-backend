package cn.edu.upc.letao.entity;

import javax.persistence.*;

/**
 * Created by cheney on 2017/4/5.
 */
@Entity
@Table(name = "lt_common_usergroup")
public class CommonUsergroup {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int groupid;

    private String type;//类型，system/special/member
    private int system;//是否可以自由进出，0是1否
    private String grouptitle;//组头衔
    private int creditshigher;//积分上限
    private int creditslower;//积分下限
    private String color;//组头衔颜色

    private int dailygold; //每日金币
    private int ingold; //发帖获得金币
    private int outgold; //淘帖减金币

    public CommonUsergroup() {
    }

    public int getGroupid() {
        return groupid;
    }

    public void setGroupid(int groupid) {
        this.groupid = groupid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getSystem() {
        return system;
    }

    public void setSystem(int system) {
        this.system = system;
    }

    public String getGrouptitle() {
        return grouptitle;
    }

    public void setGrouptitle(String grouptitle) {
        this.grouptitle = grouptitle;
    }

    public int getCreditshigher() {
        return creditshigher;
    }

    public void setCreditshigher(int creditshigher) {
        this.creditshigher = creditshigher;
    }

    public int getCreditslower() {
        return creditslower;
    }

    public void setCreditslower(int creditslower) {
        this.creditslower = creditslower;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public CommonUsergroup(String type, int system, String grouptitle, int creditshigher, int creditslower, String color) {

        this.type = type;
        this.system = system;
        this.grouptitle = grouptitle;
        this.creditshigher = creditshigher;
        this.creditslower = creditslower;
        this.color = color;
    }

    public int getDailygold() {
        return dailygold;
    }

    public void setDailygold(int dailygold) {
        this.dailygold = dailygold;
    }

    public int getIngold() {
        return ingold;
    }

    public void setIngold(int ingold) {
        this.ingold = ingold;
    }

    public int getOutgold() {
        return outgold;
    }

    public void setOutgold(int outgold) {
        this.outgold = outgold;
    }
}
