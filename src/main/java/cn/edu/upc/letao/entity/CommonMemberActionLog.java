package cn.edu.upc.letao.entity;

import javax.persistence.*;

/**
 * Created by cheney on 2017/4/5.
 */
@Entity
@Table(name = "lt_common_member_action_log")
public class CommonMemberActionLog {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int uid;
    private int type;
    private int amount;
    private String reason;
    private int time;
    private int adminid;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getAdminid() {
        return adminid;
    }

    public void setAdminid(int adminid) {
        this.adminid = adminid;
    }

    public CommonMemberActionLog() {

    }

    public CommonMemberActionLog(int uid, int type, int amount, String reason, int time, int adminid) {

        this.uid = uid;
        this.type = type;
        this.amount = amount;
        this.reason = reason;
        this.time = time;
        this.adminid = adminid;
    }
}
