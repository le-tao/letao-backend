package cn.edu.upc.letao.entity.queryVo;

import cn.edu.upc.letao.entity.CommentUpOrDown;

/**
 * Created by 风飞扬 on 2017/4/27.
 */
public class CommentUpOrDownQueryVo {

    private CommentUpOrDown commentUpOrDown;

    public CommentUpOrDown getCommentUpOrDown() {
        return commentUpOrDown;
    }

    public void setCommentUpOrDown(CommentUpOrDown commentUpOrDown) {
        this.commentUpOrDown = commentUpOrDown;
    }
}
