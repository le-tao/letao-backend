package cn.edu.upc.letao.dao;

import cn.edu.upc.letao.entity.CommonPool;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import java.util.List;

/**
 * Created by 风飞扬 on 2017/4/6.
 * 这是池子(Pool)的dao
 */
public interface CommonPoolDao extends CrudRepository<CommonPool,Integer> {
    //根据标签来查询所有池子
    public List<CommonPool> findByLabelid(int labelid);

    //查询top5池子 根据关注人数
    @Query(value = "SELECT * FROM lt_common_pool ORDER BY focusnum DESC LIMIT 5",nativeQuery = true)
    public List<CommonPool> findTop5Pools();

    //分页查询
    @Query(value = "SELECT * FROM lt_common_pool WHERE poolid LIKE %?2% OR name LIKE %?2% OR pooldesc LIKE %?2% LIMIT ?1,20",nativeQuery = true)
    public List<CommonPool> findPagePools(int page, String name);

    //池子总数
    @Query(value = "SELECT COUNT(*) FROM lt_common_pool WHERE poolid LIKE %?1% OR name LIKE %?1% OR pooldesc LIKE %?1%",nativeQuery = true)
    public Long countPools(String name);

}
