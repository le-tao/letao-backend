package cn.edu.upc.letao.dao;

import cn.edu.upc.letao.entity.CommonCollectionPool;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by 风飞扬 on 2017/6/4.
 */
public interface CollectionPoolDao extends CrudRepository<CommonCollectionPool, Integer> {
    //根据poolid和uid查询一条收藏池子记录
    public CommonCollectionPool findByPoolidAndUid(int poolid, int uid);
    //根据uid查询收藏池子记录
    public List<CommonCollectionPool> findByUid(int uid);
}
