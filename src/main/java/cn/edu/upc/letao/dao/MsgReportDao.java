package cn.edu.upc.letao.dao;

import cn.edu.upc.letao.entity.CommonReport;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by 风飞扬 on 2017/6/7.
 */
public interface MsgReportDao extends CrudRepository<CommonReport, Integer> {
    //根据消息id和举报人id查询举报记录
    public CommonReport findByMidAndUid(int mid, int uid);

    //根据消息id和被举报记录审核状态 查询举报记录
    public CommonReport findByMidAndOpresult(int mid, int opresult);

    //查询所有消息
    @Query(value = "SELECT * FROM lt_common_report limit ?1,20",nativeQuery = true)
    public List<CommonReport> findPageable(int page);

    //数所有消息
    @Query(value = "SELECT COUNT(*) FROM lt_common_report",nativeQuery = true)
    public Long countPageable();
}
