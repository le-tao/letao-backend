package cn.edu.upc.letao.dao;

import cn.edu.upc.letao.entity.CommonCollectionMessage;
import org.springframework.data.repository.CrudRepository;
import java.util.List;

/**
 * Created by 风飞扬 on 2017/6/4.
 */
public interface CollectionMsgDao extends CrudRepository<CommonCollectionMessage, Integer>{
    //根据mid和uid查询一条收藏消息记录
    public CommonCollectionMessage findByMidAndUid(int mid, int uid);

    //根据uid查询收藏消息的记录
    public List<CommonCollectionMessage> findByUid(int uid);

}
