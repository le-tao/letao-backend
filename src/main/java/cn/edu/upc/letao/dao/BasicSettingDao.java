package cn.edu.upc.letao.dao;

import cn.edu.upc.letao.entity.BasicSetting;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by 风飞扬 on 2017/6/3.
 */
public interface BasicSettingDao extends CrudRepository<BasicSetting,Integer> {
    @Query(value = "select max(id) from lt_setting",nativeQuery = true)
    public int getMaxId();
}
