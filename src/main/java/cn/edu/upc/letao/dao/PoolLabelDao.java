package cn.edu.upc.letao.dao;

import cn.edu.upc.letao.entity.PoolLabel;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by 风飞扬 on 2017/6/2.
 */

public interface PoolLabelDao extends CrudRepository<PoolLabel,Integer>{
}
