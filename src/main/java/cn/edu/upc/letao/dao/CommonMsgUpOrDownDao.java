package cn.edu.upc.letao.dao;

import cn.edu.upc.letao.entity.CommonMsgUpOrDown;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by 风飞扬 on 2017/4/18.
 */
public interface CommonMsgUpOrDownDao extends CrudRepository<CommonMsgUpOrDown,Integer> {
    //根据用户id和消息id来查询 赞或者踩的记录
    public CommonMsgUpOrDown findByUidAndMid(int uid,int mid);
}
