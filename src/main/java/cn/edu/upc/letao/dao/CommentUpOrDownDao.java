package cn.edu.upc.letao.dao;

import cn.edu.upc.letao.entity.CommentUpOrDown;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by 风飞扬 on 2017/4/27.
 */
public interface CommentUpOrDownDao extends CrudRepository<CommentUpOrDown,Integer> {
    //根据用户id和评论id来查询 赞或者踩的记录
    public CommentUpOrDown findByUidAndCommentid(int uid,int commentid);

}
