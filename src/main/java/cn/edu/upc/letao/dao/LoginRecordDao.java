package cn.edu.upc.letao.dao;

import cn.edu.upc.letao.entity.CommonLoginRecord;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import java.util.List;

/**
 * Created by 风飞扬 on 2017/6/8.
 */
public interface LoginRecordDao extends CrudRepository<CommonLoginRecord, Integer> {
    @Query(value = "SELECT * FROM lt_common_login_record WHERE uid = ? AND TIME > ?", nativeQuery = true)
    public List<CommonLoginRecord> getLoginRecordsByUidAndTime(int uid, int time);
}
