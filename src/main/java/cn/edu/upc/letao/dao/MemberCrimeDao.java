package cn.edu.upc.letao.dao;

import cn.edu.upc.letao.entity.CommonMemberCrime;
import org.springframework.data.repository.CrudRepository;
import java.util.List;

/**
 * Created by 风飞扬 on 2017/6/2.
 */

public interface MemberCrimeDao extends CrudRepository<CommonMemberCrime,Integer> {
    //根据是否 惩罚是否过期来查询
    public List<CommonMemberCrime> findByExpired(int expired);
}
