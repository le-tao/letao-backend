package cn.edu.upc.letao.dao;

import cn.edu.upc.letao.entity.CommonUsergroup;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import java.util.List;

/**
 * Created by 风飞扬 on 2017/6/6.
 */
public interface GroupDao extends CrudRepository<CommonUsergroup, Integer> {
    @Query(value = "SELECT * FROM lt_common_usergroup", nativeQuery = true)
    public List<CommonUsergroup> getAllGroups();

    @Query(value = "SELECT * FROM lt_common_usergroup WHERE creditslower <= ? AND (creditshigher > ? OR creditshigher=-1)", nativeQuery = true)
    public CommonUsergroup getProperGroup(int exp1,int exp2);


}
