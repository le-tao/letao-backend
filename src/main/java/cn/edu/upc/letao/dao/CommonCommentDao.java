package cn.edu.upc.letao.dao;

import cn.edu.upc.letao.entity.CommonComment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import java.util.List;

/**
 * Created by 风飞扬 on 2017/4/20.
 * 这是评论的dao
 */
public interface CommonCommentDao extends CrudRepository<CommonComment,Integer> {

    public CommonComment findByCommentid(int commentid);

    public List<CommonComment> findByMid(int mid);

    //查询所有消息
    @Query(value = "SELECT * FROM lt_common_comment WHERE text LIKE %?2% limit ?1,20",nativeQuery = true)
    public List<CommonComment> findPageable(int page,String name);

    //数所有消息
    @Query(value = "SELECT COUNT(*) FROM lt_common_comment WHERE text LIKE %?1%",nativeQuery = true)
    public Long countPageable(String name);
}
