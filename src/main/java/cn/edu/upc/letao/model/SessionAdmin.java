package cn.edu.upc.letao.model;

/**
 * Created by cheney on 2017/4/5.
 */
public class SessionAdmin {
    public long admin_visit_time;
    public String admin_visit_token;//作废
    public String admin_visit_user_name;
    public int admin_visit_user_id;

    public SessionAdmin() {
    }

    public long getAdmin_visit_time() {

        return admin_visit_time;
    }

    public void setAdmin_visit_time(long admin_visit_time) {
        this.admin_visit_time = admin_visit_time;
    }

    public String getAdmin_visit_token() {
        return admin_visit_token;
    }

    public void setAdmin_visit_token(String admin_visit_token) {
        this.admin_visit_token = admin_visit_token;
    }

    public String getAdmin_visit_user_name() {
        return admin_visit_user_name;
    }

    public void setAdmin_visit_user_name(String admin_visit_user_name) {
        this.admin_visit_user_name = admin_visit_user_name;
    }

    public int getAdmin_visit_user_id() {
        return admin_visit_user_id;
    }

    public void setAdmin_visit_user_id(int admin_visit_user_id) {
        this.admin_visit_user_id = admin_visit_user_id;
    }

    public SessionAdmin(long admin_visit_time, String admin_visit_token, String admin_visit_user_name, int admin_visit_user_id) {

        this.admin_visit_time = admin_visit_time;
        this.admin_visit_token = admin_visit_token;
        this.admin_visit_user_name = admin_visit_user_name;
        this.admin_visit_user_id = admin_visit_user_id;
    }
}
