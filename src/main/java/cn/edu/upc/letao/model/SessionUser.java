package cn.edu.upc.letao.model;

/**
 * Created by cheney on 2017/4/5.
 */
public class SessionUser {
    public long visit_time;
    public String visit_token;//作废
    public String visit_user_name;
    public int visit_user_id;

    public long getVisit_time() {
        return visit_time;
    }

    public void setVisit_time(long visit_time) {
        this.visit_time = visit_time;
    }

    public String getVisit_token() {
        return visit_token;
    }

    public void setVisit_token(String visit_token) {
        this.visit_token = visit_token;
    }

    public String getVisit_user_name() {
        return visit_user_name;
    }

    public void setVisit_user_name(String visit_user_name) {
        this.visit_user_name = visit_user_name;
    }

    public int getVisit_user_id() {
        return visit_user_id;
    }

    public void setVisit_user_id(int visit_user_id) {
        this.visit_user_id = visit_user_id;
    }

    public SessionUser() {

    }

    public SessionUser(long visit_time, String visit_token, String visit_user_name, int visit_user_id) {
        this.visit_time = visit_time;
        this.visit_token = visit_token;
        this.visit_user_name = visit_user_name;
        this.visit_user_id = visit_user_id;
    }

}
