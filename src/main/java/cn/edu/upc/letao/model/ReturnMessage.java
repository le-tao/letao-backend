package cn.edu.upc.letao.model;

/**
 * Created by cheney on 2017/4/6.
 */
public class ReturnMessage {
    public int id;
    public Object message;

    public ReturnMessage() {
    }

    public ReturnMessage(int id, Object message) {

        this.id = id;
        this.message = message;
    }


}
