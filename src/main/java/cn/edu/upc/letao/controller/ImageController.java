package cn.edu.upc.letao.controller;

import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by youweirong on 2017/4/26.
 */
@RestController
@RequestMapping("/api")
public class ImageController {

	@Value("${qiniu.cheney.AK}")
	String accessKey;
	@Value("${qiniu.cheney.SK}")
	String secretKey;
	@RequestMapping("/image")
	public Object upImage(){//上传头像
		//构造一个带指定Zone对象的配置类
		Configuration cfg = new Configuration(Zone.zone0());
        //...其他参数参考类注释
		UploadManager uploadManager = new UploadManager(cfg);
        //...生成上传凭证，然后准备上传
		String bucket = "letao";
        //如果是Windows情况下，格式是 D:\\qiniu\\test.png
		String localFilePath = "/Users/youweirong/Desktop/me.jpeg";
        //默认不指定key的情况下，以文件内容的hash值作为文件名
		String key = "test/ywr/me.jpg";
		Auth auth = Auth.create(accessKey, secretKey);
		String upToken = auth.uploadToken(bucket);
		try {
			Response response = uploadManager.put(localFilePath, key, upToken);
			//解析上传成功的结果
			DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
			System.out.println(putRet.key);
			System.out.println(putRet.hash);
			return response;
		} catch (QiniuException ex) {
			Response r = ex.response;
			System.err.println(r.toString());
			try {
				System.err.println(r.bodyString());
				return r;
			} catch (QiniuException ex2) {
				//ignore
				return ex2;
			}
		}

	}
}
