package cn.edu.upc.letao.controller;

import cn.edu.upc.letao.dao.CommonMemberDao;
import cn.edu.upc.letao.entity.CommonMember;
import cn.edu.upc.letao.model.ReturnMessage;
import cn.edu.upc.letao.service.AuthService;
import cn.edu.upc.letao.service.UserinfoService;
import cn.edu.upc.letao.util.MyFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by youweirong on 2017/4/11.
 */

@RestController
@RequestMapping("/api")
public class UserinfoController {
	@Autowired
	public UserinfoService userinfoService;
	@Autowired
	public AuthService authService;
	@Autowired
	private CommonMemberDao commonMemberDao;
	@Autowired
	private MyFileUpload mu;//注入上传文件的类

	@RequestMapping("/userinfo")
	public Object getOnemenber(){//显示个人信息
		class UserInfo{
			public boolean isLogin;
			public int uid;
			public String username;
            public String email;
            public int emailstatus;
            public int status;
            public int exp;
            public int gold;
			public String filepath;
		}
		ReturnMessage returnMessage = new ReturnMessage();
		UserInfo userInfo = new UserInfo();
		userInfo.isLogin = authService.isAuthenticated();
		CommonMember c = userinfoService.getCurrentUser();
		if(c!=null){
			userInfo.uid = c.getUid();
			userInfo.username = c.getUsername();
			userInfo.email = c.getEmail();
			userInfo.emailstatus = c.getEmailstatus();
			userInfo.status = c.getStatus();
			userInfo.exp = c.getExp();
			userInfo.gold = c.getGold();
			userInfo.filepath=c.getFilepath();
			returnMessage.id = 0;
			returnMessage.message = userInfo;
		}else{
			returnMessage.id = 1;
			returnMessage.message = "未登录！";
		}
		return returnMessage;
	}

	@RequestMapping("/edituserinfo")
		public Object editUserinfo(String uname, String email, HttpServletResponse response){//编辑个人信息
		CommonMember c = userinfoService.editCurrentUser();
		c.setUsername(uname);
		//c.setEmail(email);
		c.setEmailstatus(0);
		commonMemberDao.save(c);
		ReturnMessage returnMessage = new ReturnMessage();
		returnMessage.id = 0;
		returnMessage.message = "修改成功";
		return returnMessage;
	}

	@RequestMapping("/changepassword")
	public Object editCurrentPassword(String password, HttpServletResponse response){//修改密码
		CommonMember c = userinfoService.editCurrentUser();
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		c.setPassword(passwordEncoder.encode(password));
		commonMemberDao.save(c);
		ReturnMessage returnMessage = new ReturnMessage();
		returnMessage.id = 0;
		returnMessage.message = "修改成功";
		return returnMessage;
	}

	@RequestMapping("/changeimage")
	public Object editCurrentFilepath(HttpServletRequest request,Integer uid){//修改头像
		ReturnMessage returnMessage = new ReturnMessage();
		if(true){
			//messageService.saveMessage(messageQuery.getCommonMessage());
			//添加多文件上传支持
			List<MultipartFile> files = ((MultipartHttpServletRequest) request)
					.getFiles("file");
			if(files!=null){
				//MyFileUpload mu = new MyFileUpload();
				//调用封装过的文件上传方法 成功的话，文件的路径 返回在ReturnMessage的message中
				//如果是多文件的话，文件路径逗号隔开
				ReturnMessage myMessage = mu.multiFileUpload(files);
				if(myMessage.id==1){//失败
					returnMessage.id = 1;
					returnMessage.message = "操作失败";
				}else{//成功
					//CommonMember c = userinfoService.editCurrentUser();
					//找到这个人
					CommonMember c = commonMemberDao.findOne(uid);
					//设置路径
					c.setFilepath((String)myMessage.message);
					returnMessage.id = 0;
					returnMessage.message = "操作成功";
					commonMemberDao.save(c);
				}
			}
		}else{
			returnMessage.id = 1;
			returnMessage.message = "操作失败";
		}
		return returnMessage;
	}
}
