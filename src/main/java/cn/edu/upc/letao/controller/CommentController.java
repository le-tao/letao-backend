package cn.edu.upc.letao.controller;

import cn.edu.upc.letao.entity.CommonComment;
import cn.edu.upc.letao.entity.queryVo.CommentQueryVo;
import cn.edu.upc.letao.entity.queryVo.CommentUpOrDownQueryVo;
import cn.edu.upc.letao.model.ReturnMessage;
import cn.edu.upc.letao.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 * Created by 风飞扬 on 2017/4/25.
 */
@RestController
@RequestMapping("/comment")
public class CommentController {
    @Autowired
    private CommentService commentService;

    //根据消息id查看所有评论
    @RequestMapping(value = "/getComments")
    public List<CommonComment> getComments(Integer mid){
        return commentService.getCommentsByMid(mid);
    }

    //添加评论
    @RequestMapping(value = "/addComment")
    public ReturnMessage addComment(CommentQueryVo queryVo){
        ReturnMessage returnMessage = new ReturnMessage();
        if(queryVo!=null&&queryVo.getCommonComment()!=null){
            boolean b = commentService.addComment(queryVo.getCommonComment());
            if(b){
                returnMessage.id = 0;
                returnMessage.message="操作成功";
            }else{
                returnMessage.id = 1;
                returnMessage.message="操作失败";
            }
        }else{
            returnMessage.id = 1;
            returnMessage.message="操作失败";
        }
        return returnMessage;
    }
    //删除评论
    @RequestMapping(value = "/delComment")
    public ReturnMessage delComment(int commentid){
        ReturnMessage returnMessage = new ReturnMessage();
        boolean b = commentService.deleteComment(commentid);
        if(b){
            returnMessage.id = 0;
            returnMessage.message = "操作成功";
        }else{
            returnMessage.id = 1;
            returnMessage.message = "操作失败";
        }
        return returnMessage;
    }

    //更新评论赞或者踩的数量
    //传参格式 commentUpOrDown.uid  commentUpOrDown.commentid commentUpOrDown.myflag
    @RequestMapping(value = "addUpOrDown")
    public ReturnMessage addUpOrDown(CommentUpOrDownQueryVo queryVo){
        ReturnMessage returnMessage = new ReturnMessage();
        if(queryVo!=null){
            boolean b = commentService.addUpOrDown(queryVo.getCommentUpOrDown());
            if(b){
                returnMessage.id = 1;//此时页面应该更新赞或者踩的数量
                returnMessage.message = "页面更新赞或者踩的数量";
            }else{
                returnMessage.id = 0;
                returnMessage.message = "页面不需要更新赞或者踩的数量";
            }
        }
        return returnMessage;
    }

}
