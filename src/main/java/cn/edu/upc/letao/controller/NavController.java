package cn.edu.upc.letao.controller;

import cn.edu.upc.letao.model.ReturnMessage;
import cn.edu.upc.letao.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by cheney on 2017/4/6.
 */
@RestController
@RequestMapping("/api")
public class NavController {

    @Autowired
    private AuthService authService;

    @RequestMapping("/nav")
    public Object nav(){
        ReturnMessage returnMessage = new ReturnMessage();
        class NavInfo{
            public boolean isLogin;
            public int uid;
            public String username;
//            public String email;
//            public int emailstatus;
//            public int status;
//            public int exp;
//            public int gold;
        }
        NavInfo navInfo = new NavInfo();
        navInfo.isLogin = authService.isAuthenticated();
        if(navInfo.isLogin){
            navInfo.username = authService.currentUser().getVisit_user_name();
            navInfo.uid = authService.currentUser().visit_user_id;
        }else{
            navInfo.uid = -1;
            navInfo.username = "游客";
        }
        returnMessage.id = 0;
        returnMessage.message = navInfo;
        return returnMessage;
    }
}
