package cn.edu.upc.letao.controller;

import cn.edu.upc.letao.model.ReturnMessage;
import cn.edu.upc.letao.service.AuthService;
import cn.edu.upc.letao.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by cheney on 2017/4/6.
 */
@RestController
@RequestMapping("api")
public class LoginController {
    @Autowired
    private LoginService loginService;
    @Autowired
    private AuthService authService;

    @RequestMapping("login")
    public Object loginVerify(String email, String password) throws IOException {
        ReturnMessage returnMessage = new ReturnMessage();
        if(loginService.login(email,password)){
            //登录成功
            returnMessage.id = 0;
            returnMessage.message = "登录成功";
        }else{
            //登录失败
            returnMessage.id = 1;
            returnMessage.message = "登录失败，请检查邮箱和密码";
        }

        return returnMessage;
    }

    @RequestMapping("register")
    public Object registerVerify(String email, String password, HttpServletResponse response) throws IOException {
        int demo=1;
        System.out.println(email+password);                   //并没有输出email+password？？？？？？？？？？？？？？？？？？？？？？？？？
        ReturnMessage returnMessage = new ReturnMessage();
        if(loginService.register(email,password)){
            //注册成功
            returnMessage.id = 0;
            returnMessage.message = "注册成功";
        }else{
            //登录失败
            returnMessage.id = 1;
            returnMessage.message = "注册失败，请检查输入";
        }
        return returnMessage;
    }

    @RequestMapping("logout")
    public Object logout(){
        ReturnMessage returnMessage = new ReturnMessage();
        authService.invalidLogin();
        returnMessage.id = 0;
        returnMessage.message = "退出成功";
        return returnMessage;
    }
}
