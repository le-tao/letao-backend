package cn.edu.upc.letao.controller;

import cn.edu.upc.letao.entity.CommonMember;
import cn.edu.upc.letao.model.ReturnMessage;
import cn.edu.upc.letao.service.UserinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by cheney on 2017/2/27.
 */
@RestController
public class IndexController {

	public String showIndex() {
		return "home";
	}
}

