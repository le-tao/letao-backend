package cn.edu.upc.letao.controller;

import cn.edu.upc.letao.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by 风飞扬 on 2017/6/7.
 */
@RestController
@RequestMapping("/user")
public class MemberController {
    @Autowired
    private MemberService memberService;

    @RequestMapping(value = "getTop5Members")
    public Object getTop5Members(){
        return memberService.getTop5Memeber();
    }

}
