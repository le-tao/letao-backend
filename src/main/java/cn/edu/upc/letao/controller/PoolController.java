package cn.edu.upc.letao.controller;

import cn.edu.upc.letao.entity.CommonPool;
import cn.edu.upc.letao.model.ReturnMessage;
import cn.edu.upc.letao.model.SessionUser;
import cn.edu.upc.letao.service.AuthService;
import cn.edu.upc.letao.service.MemberService;
import cn.edu.upc.letao.service.PoolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by 风飞扬 on 2017/4/6.
 * 这是池子（Pool）的控制器
 */
@RestController
@RequestMapping("pool")
public class PoolController {

    @Autowired
    private PoolService poolService;
    @Autowired
    private AuthService authService;
    @Autowired
    private MemberService memberService;
    @Autowired
    private HttpSession httpSession;


    //获取所有池子
    @RequestMapping("getall")
    public Object getAll(){
        return poolService.getAll();
    }

    //根据池子标签查询池子
    @RequestMapping("getByLabel")
    public Object getByLabel(Integer labelid){
        List<CommonPool> pools = poolService.findPoolsByLableid(labelid);
        return pools;
    }
    //关注池子
    @RequestMapping(value = "focusPool")
    public Object focusPool(Integer uid, Integer poolid){
        ReturnMessage returnMessage = new ReturnMessage();
        SessionUser user = authService.currentUser();
        //boolean b = poolService.focusPool(poolid, user.getVisit_user_id());
        boolean b = poolService.focusPool(poolid, uid);
        if(b){
            returnMessage.id = 1;
            returnMessage.message = "关注成功";
        }else{
            returnMessage.id = 0;
            returnMessage.message = "关注失败";
        }

        return returnMessage;
    }
    //取消关注
    @RequestMapping(value = "cancelFocus")
    public Object cancelFocus(Integer uid, Integer poolid){
        ReturnMessage returnMessage = new ReturnMessage();
        SessionUser user = authService.currentUser();
        //boolean b = poolService.cancelFocusPool(poolid, user.getVisit_user_id());
        boolean b = poolService.cancelFocusPool(poolid, uid);
        if(b){
            returnMessage.id = 1;
            returnMessage.message = "取消关注成功";
        }else{
            returnMessage.id = 0;
            returnMessage.message = "取消关注失败";
        }


        return returnMessage;
    }
    //推荐池子
    @RequestMapping(value = "recommendPool")
    public Object recommendPool(Integer uid, Integer labelid){
        SessionUser user = authService.currentUser();
        //List<CommonPool> pools = poolService.recommendPool(user.getVisit_user_id(), labelid);
        List<CommonPool> pools = poolService.recommendPool(uid, labelid);
        return pools;
    }
    //判断是否已经关注了池子
    @RequestMapping(value = "isFocus")
    public Object isFocus(Integer uid, Integer poolid){
        ReturnMessage returnMessage = new ReturnMessage();
        boolean b = poolService.isFocus(uid, poolid);
        if(b){
            returnMessage.id = 1;
            returnMessage.message = "已关注";
        }else{
            returnMessage.id = 0;
            returnMessage.message = "未关注";
        }
        return returnMessage;
    }

    //查询top5的池子
    @RequestMapping(value = "getTop5Pools")
    public Object getTop5Pools(){
        String str = "top5Pools";
        return poolService.getTop5Pools(str);
    }

    //查询用户 关注的池子
    @RequestMapping(value = "getAllFocusPools")
    public Object getAllFocusPools(Integer uid){
        return poolService.getAllFocusPools(uid);
    }

    @RequestMapping(value = "getPoolById")
    public Object getPoolById(Integer poolid){
        return poolService.findOne(poolid);
    }


}
