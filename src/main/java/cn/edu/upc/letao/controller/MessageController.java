package cn.edu.upc.letao.controller;

import cn.edu.upc.letao.entity.CommonMessage;
import cn.edu.upc.letao.entity.custom.CommonMessageCustom;
import cn.edu.upc.letao.entity.queryVo.CommonMessageQueryVo;
import cn.edu.upc.letao.entity.queryVo.CommonMsgUpOrDownQueryVo;
import cn.edu.upc.letao.model.ReturnMessage;
import cn.edu.upc.letao.service.GoldService;
import cn.edu.upc.letao.service.MessageService;
import cn.edu.upc.letao.service.MsgReportService;
import cn.edu.upc.letao.util.MyFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by 风飞扬 on 2017/4/5.
 *
 * 这是有关消息Message的控制器
 */
@RestController
@RequestMapping("messages")
public class MessageController {
    @Autowired
    private MessageService messageService;

    //注入文件上传方法对象
    @Autowired
    private MyFileUpload mu;

    @Autowired
    private MsgReportService msgReportService;
    @Autowired
    private GoldService goldService;


    //淘出一条消息
    @RequestMapping(value = "queryOneMessage")
    public CommonMessage queryOneMessage(int poolid){

        return messageService.queryOneMessage(poolid);
    }

    //淘出一条消息 需要登录uid和poolid
    @RequestMapping(value = "queryOneMessageByUidAndPoolid")
    public Object queryOneMessageByUidAndPoolid(Integer poolid, Integer uid){

        CommonMessageCustom messageCustom = messageService.queryOneMessageByUidAndPoolId(poolid, uid);
        if(messageCustom.getHasgold()==0){
            CommonMessageCustom newMessageCustom = new CommonMessageCustom();
            newMessageCustom.setHasgold(messageCustom.getHasgold());
            return newMessageCustom;
        }
        return messageCustom;
    }

    @RequestMapping(value = "getMsgById")
    public CommonMessage getMsgById(Integer mid){
        return messageService.getMsgById(mid);
    }


    //保存或者更新一条消息
    //示范参数 http://localhost:8081/messages/saveMessage
    //?commonMessage.date=1&commonMessage.poolid=1&commonMessage.text=hehehe&commonMessage.uid=1
    //支持多文件上传
    //上传文件时候  指定enctype="multipart/form-data"  <input type="file" name="file" /> name指定为file
    @RequestMapping(value = "saveMessage")
    public ReturnMessage saveMessage(CommonMessageQueryVo messageQuery, HttpServletRequest request, Integer uid){
        ReturnMessage returnMessage = new ReturnMessage();
        if(messageQuery!=null&&messageQuery.getCommonMessage()!=null){
            //messageService.saveMessage(messageQuery.getCommonMessage());
            //添加多文件上传支持
            List<MultipartFile> files = ((MultipartHttpServletRequest) request)
                    .getFiles("file");
            if(files!=null){
                //MyFileUpload mu = new MyFileUpload();
                //调用封装过的文件上传方法 成功的话，文件的路径 返回在ReturnMessage的message中
                //如果是多文件的话，文件路径逗号隔开
                ReturnMessage myMessage = mu.multiFileUpload(files);
                if(myMessage.id==1){//失败
                    returnMessage.id = 1;
                    returnMessage.message = "操作失败";
                }else{//成功
                    //设置路径
                    if(myMessage.message!=null&&((String)myMessage.message).length()>=1){
                        messageQuery.getCommonMessage().setFilepath((String) myMessage.message);
                    }
                    returnMessage.id = 0;
                    returnMessage.message = "操作成功";
                }
            }

            messageService.saveMessage(messageQuery.getCommonMessage(),uid);
            //添加多文件上传支持

        }else{
            returnMessage.id = 1;
            returnMessage.message = "操作失败";
        }
        return returnMessage;
    }

    //更新消息赞或者踩的数量
    //传参格式commonMsgUpOrDown.uid   commonMsgUpOrDown.mid  commonMsgUpOrDown.myflag
    @RequestMapping(value = "addUpOrDown")
    public ReturnMessage addUpOrDown(CommonMsgUpOrDownQueryVo queryVo){
        ReturnMessage returnMessage = new ReturnMessage();
        if(queryVo!=null){
            boolean b = messageService.addUpOrDown(queryVo.getCommonMsgUpOrDown());
            if(b){
                returnMessage.id = 1;//此时页面应该更新赞或者踩的数量
                returnMessage.message = "页面更新赞或者踩的数量";
            }else{
                returnMessage.id = 0;//此时页面不需要改变赞或者踩的数量
                returnMessage.message = "页面不需要更新赞或者踩的数量";
            }
        }
        return returnMessage;
    }

    //判断消息是否被收藏
    @RequestMapping(value = "isCollect")
    public Object isCollect(Integer uid, Integer mid){
        ReturnMessage returnMessage = new ReturnMessage();
        boolean b = messageService.isCollect(uid, mid);
        if(b) {
            returnMessage.id = 1;
            returnMessage.message = "已经收藏";
        }else{
            returnMessage.id = 0;
            returnMessage.message = "没有收藏";
        }
        return returnMessage;
    }
    //推荐 top10消息
    @RequestMapping(value = "recommendTop10Msgs")
    public Object recommendTop10Msgs(Integer poolid){
        List<CommonMessage> top10Msgs = messageService.recommendTop10Msgs(poolid);
        return top10Msgs;
    }

    //判断用户是否已经赞过某条消息
    //-1表示没有赞或者踩 0表示已经踩过， 1表示已经赞过
    @RequestMapping(value = "isUpOrDown")
    public Object isUpOrDown(Integer mid, Integer uid){
        ReturnMessage returnMessage = new ReturnMessage();
        int myflag = messageService.isUpOrDown(mid, uid);
        switch(myflag){
            case -1:
                returnMessage.id = -1;
                returnMessage.message = "没有赞或者踩";
                break;
            case 0:
                returnMessage.id = 0;
                returnMessage.message = "已经踩过";
                break;
            case 1:
                returnMessage.id = 1;
                returnMessage.message = "已经赞过";
                break;
        }
        return returnMessage;
    }

    //收藏某条消息
    @RequestMapping(value = "collectMsg")
    public Object collectMsg(Integer uid, Integer mid){
        ReturnMessage returnMessage = new ReturnMessage();
        boolean b = messageService.collecMsg(uid, mid);
        if(b){
            returnMessage.id = 1;
            returnMessage.message = "收藏成功";
        }else{
            returnMessage.id = 0;
            returnMessage.message = "收藏失败";
        }
        return returnMessage;
    }
    //取消收藏某条消息
    @RequestMapping(value = "cancelCollectMsg")
    public Object cancelCollectMsg(Integer uid, Integer mid){
        ReturnMessage returnMessage = new ReturnMessage();
        boolean b = messageService.cancelCollectMsg(mid, uid);
        if(b){
            returnMessage.id = 1;
            returnMessage.message = "取消收藏成功";
        }else{
            returnMessage.id = 0;
            returnMessage.message = "取消收藏失败";
        }
        return returnMessage;
    }

    //top5
    @RequestMapping(value = "getTop5Msgs")
    public Object getTop5Msgs(){
        String top5Msgs = "top5Msgs";
<<<<<<< HEAD
       return  messageService.getTop5Msgs(top5Msgs);
=======
        return  messageService.getTop5Msgs(top5Msgs);
>>>>>>> 656d1e0fc23ec798a8dba36f017a70cd2371a127
    }

    //查询用户  所有的 已经收藏的消息
    @RequestMapping(value = "getAllCollectMsgs")
    public Object getAllCollectMsgs(Integer uid){
<<<<<<< HEAD
       return  messageService.getAllCollectMsgs(uid);
=======
        return  messageService.getAllCollectMsgs(uid);
>>>>>>> 656d1e0fc23ec798a8dba36f017a70cd2371a127
    }

    //举报消息
    @RequestMapping(value = "reportMsg")
    public Object reportMsg(Integer uid, Integer mid){
        ReturnMessage returnMessage = new ReturnMessage();
        boolean b = msgReportService.reportMsg(uid, mid);
        if(b){
            returnMessage.id = 1;
            returnMessage.message = "举报成功!";
        }else{
            returnMessage.id = 0;
            returnMessage.message = "举报失败!";
        }
        return returnMessage;
    }
    //取消举报消息
    @RequestMapping(value = "cancleReportMsg")
    public Object cancleReportMsg(Integer uid,Integer mid){
        ReturnMessage returnMessage = new ReturnMessage();
        boolean b = msgReportService.cancleReportMsg(uid, mid);
        if(b){
            returnMessage.id = 1;
            returnMessage.message = "取消举报成功";
        }else{
            returnMessage.id = 0;
            returnMessage.message = "取消举报失败";
        }
        return returnMessage;
    }
    //判断用户是否举报过某条消息
    @RequestMapping(value = "isReportMsg")
    public Object isReportMsg(Integer uid, Integer mid){
        ReturnMessage returnMessage = new ReturnMessage();
        boolean b = msgReportService.isReport(uid, mid);
        if(b){
            returnMessage.id = 1;
            returnMessage.message = "已经举报过了";
        }else{
            returnMessage.id = 0;
            returnMessage.message = "没有举报过";
        }
        return returnMessage;
    }

    @RequestMapping(value = "isHasGold")
    public Object isHasGold(Integer poolid, Integer uid){
        ReturnMessage returnMessage = new ReturnMessage();
        boolean b = goldService.isHaveGold(uid);
        if(b){
            returnMessage.id = 1;
            returnMessage.message = "积分够了能淘消息";
        } else {
            returnMessage.id = 0;
            returnMessage.message = "积分不够不可以淘消息";
        }
        return returnMessage;
    }
}

